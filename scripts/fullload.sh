arcrad graph load --source arcrad_mittwoch  --force --verbose
arcrad graph fusion --graph out_arcrad_oldradicali out_arcrad_radicali out_arcrad_mittwoch out_arcrad_cora out_arcrad_cora out_arcrad_exagora --db-out out_arcrad_fusion
arcrad graph run clean titles --graph out_arcrad_fusion --output modif.csv --force
arcrad graph dump --graph out_arcrad_fusion --output xfusion.xml 


# INFO:pyarcrad:[arcrad_exagora] in 4095.946896 sec : fatti 1832499/4095
 
