"""

1. Import mysql dump of a source
?   >>> arcrad source import --db-out arcrad_exgora --infile exagora.dump

2. Load source into graph 
*   >>> arcrad graph load --source arcrad_exagora --db-out graph_exagora --reset-cache --force

2.1 Unify multiple graphs 
*   >>> arcrad graph fusion --graph graph_first graph_second --db-out fusion_graph

2.2 Clean 
*   >>> arcrad graph run clean titles --graph out_arcrad_fusion --output modif.csv --force

3. Export graph into xml dump for wiki
*   >>> arcrad graph dump --source graph_exagora 

4. Wiki restore
   >>> arcrad wiki restore --file output*.xml

5. Wiki export news
   >>> arcrad wiki export --news --out news.xml


"""
import argparse
import cmd
import os
import re
import sys
import time
import traceback
import unittest
from collections import defaultdict
from datetime import datetime
from hashlib import sha224
from os.path import exists, expanduser, expandvars
from pathlib import Path
from pprint import pformat, pprint

import IPython
from bs4 import BeautifulSoup
from rdflib import Namespace, URIRef

from . import LOGGER, ArchivioRadicaleP
from .archive import Corpus, Recognizers
from .consumer import KwConsumer
from .cursor import MysqlDB, mysql_drop_all_tables, mysql_send_file
from .db import graph_elements_loader, load_source, speed_dump, update_db, convoy_source
from .exporter import KwCorpusExporter
from .graph import SQLAlchemyTripleStore, TripleStore, load_archive, upload_archive
from .utils import PRNTP, FilePattern, Progress, Timer, setup_parms

HISTORYFILE='~/.pyarcrad_history'

class LocalMediaWiki(object):
    def __init__(path):
        self.path = Path(path)

class Command(object):
    pass

class get(Command):
    def __call__(self, archive,pid):
        print("Loading document n. %s in archive %s" % (pid,archive))
        if archive in Recognizers:
            fonte = Recognizers[archive]()
            print('Archive: %s' % fonte.SOURCE)
            return fonte.get(pid)

class triples(Command):
    def __call__(self,archive,pid):
        if archive in Recognizers:
            print("Loading triples n. %s in archive %s" % (pid,archive))
            fonte = Recognizers[archive]()
            return fonte.get_triples(pid)

class ids(Command):
    def __call__(self,archive):
        if archive in Recognizers:
            fonte = Recognizers[archive]()
            return fonte.ids

class commands(Command):
    def __call__(self):
        cmds = ' '.join(list(
            map(lambda x: x[0],
                filter( lambda obj: isinstance(obj[1],type) and \
                        issubclass(obj[1], Command) and \
                        obj[0] != 'Command' , globals().items() ))))
        print(cmds)

class loadall(Command):
    def __call__(self, line):
        pass

def d(*args, **kwargs):
    return (args, kwargs)

# Arguments Bank
ArgumentBank =  {
    'also': d("--also", nargs='+', type=str, help="additional informations"),
    'arc': d("arc", nargs='+', type=str, help="command"),
    'bind': d("--bind", type=str, nargs='*', dest='bind', help="Bindings", default=None),
    'cmd' : d("cmd", nargs='+', type=str, help="command"),
    'config' : d('-C',"--config", type=str, dest='config', help="file", default='~/Dropbox/code/archivio-radicale/wikis/w/LocalSettings.php'),
    'create' : d("--create", help="create", action="store_true"),
    'db' : d("--db", nargs='+', type=str, dest='db', help="database label", default=None),
    'db-out' : d("--db-out", type=str, dest='dbout', help="create new database", default=None),
    'encoding': d("--encoding", type=str, dest='encoding',help="text encoding", default='utf-8'),
    'fields' : d("-F","--fields", nargs='+', choices=["o","p","s"],type=str, dest='fields', help="fields", default=["s","p","o"]),
    'file': d('-f',"--file", type=str, dest='file', help="file", default='output.xml'),
    'filter' : d("--filter", nargs='?', type=str, dest='filter', help="subject", default=None),
    'force' : d("-f", "--force", help="force file creation", action="store_true"),
    'format': d('--format', help="formats", dest="format", choices=[ 'xml', 'n3', 'turtle', 'nt', 'pretty-xml', 'trix' ], default='turtle'),
    'glob' : d("--glob", nargs='?', type=str, dest='glob', help="glob", default=None),
    'graph' : d("--graph", nargs='+', type=str, dest='graph', help="graph name", default=[]),
    'howmany' : d("-H","--howmany", type=int, dest='howmany', help="howmany", default=None),
    'infile' : d("--infile", type=argparse.FileType('r'), dest='infile', help="input file", default=None),
    'limit' : d("--limit", type=int, dest='limit', help="number of posts in xml", default=2500),
    'keys' : d("-K","--keys", nargs='+', type=str, dest='keys', help="keys", default=None),
    'output' : d("-O", "--output", type=str, dest='output', help="output csv file", default=None),
    'ns': d("--ns", type=str, nargs='*', dest='ns', help="Namespaces", default=[]),
    'output-csv' : d("--format", type=str, dest='format', help="output format file", default='csv'),
    'output-format' : d("--format", type=str, dest='format', help="output format file", default='csv'),
    'output-txt' : d("-O", "--output", type=str, dest='output', help="output text file", default='output.txt'),
    'obj' : d("-Z","--obj", nargs='?', type=str, dest='obj', help="object", default=None),
    'pred' : d("-Y","--pred", nargs='?', type=str, dest='pred', help="predicate", default=None),
    'percent' : d("--percent", type=int, dest='percent', help="10=10%%", default=10),
    'publicid' : d("--publicid", nargs='?', type=str, dest='publicid', help="publicid", default=None),
    'reset-cache' : d("--reset-cache", help="reset-cache", action="store_true", dest="reset_cache"),
    'separator' : d("-S","--separator", nargs='?', type=str, dest='sep', help="separator", default=";"),
    'source' : d("--source", nargs='+', type=str, dest='source', help="source database label", default=None),
    'sparql' : d("--sparql", type=argparse.FileType('r'), dest='sparql', help="SPARQL Query", default=None),
    'sql' : d("--sql", nargs='+', type=str, dest='sql', help="SQL Query", default=None),
    'subj' : d( "-X","--subj", nargs='?', type=str, dest='subj', help="subject", default=None),
    'subj-file' : d( "-X","--subj", nargs='?', type=argparse.FileType('r'), dest='subj', help="subject", default=None),
    'verbose' : d("-v", "--verbose", help="increase output verbosity", action="store_true"),
    'xml-files': d('--xml-files', nargs="+", type=str, dest='xmlfiles', help="xml files to import into wiki", default=[]),
}

def setup_argument_parser(*args):
    parser = argparse.ArgumentParser()
    arguments = dict(filter(lambda x: x[0] in args, ArgumentBank.items()))
    for arg in arguments.values():
        parser.add_argument(*arg[0],**arg[1])
    return parser

def get_arguments(line,*args):
    parser = setup_argument_parser(*args)
    args=parser.parse_args(args=line.split())
    return args

def export_elements(args,_els,output=None):
    if hasattr(args,'dbout') and args.dbout:
        LOGGER.info('Exporting in db %s', args.dbout)
        export_elements_in_db(args, _els)
    elif hasattr(args,'output') and args.output:
        if output:
            args.output = output
        LOGGER.info('Exporting in file %s', args.output)
        export_elements_in_xsv(args, _els)
    else:
        pprint(_els)

def export_elements_in_db(args, _els):
    store = SQLAlchemyTripleStore(label=args.dbout, create=args.create, force=args.force)
    store.add_dict('arcrad',_els)
    print("# results %d %s" % (len(_els),
                               "" if len(_els)==0 else "(in %s)" % args.dbout))

def export_elements_in_xsv(args,_els):
    """User args.fname, args.fields, args.sep"""
    if _els and len(_els)>0:
        path = Path(args.output)
        if path.exists(): path.unlink()
        with path.open('wb') as fh:
            fields = []
            fields.append(args.fields)
            for _k1,_e1 in _els.items():
                for _k2, _v in _e1.items():
                    val = []
                    if "s" in args.fields:
                        val.append(_k1)
                    if "p" in args.fields:
                        val.append(_k2)
                    if "o" in args.fields:
                        val.append('|'.join(_v))
                    fields.append(val)
            for field in fields:
                fh.write((args.sep.join(field)+"\n").encode('utf-8'))
    print("# results %d %s" % (len(_els),
                               "" if len(_els)==0 else "(in %s)" % args.output))

def export_sql(format, fname, results):
    LOGGER.info("Exporting %d results in file %s (format=%s)" % (len(results),fname,results))
    if format in ('csv',):
        export_sql_in_csv(fname,results)

def export_sql_in_csv(fname, results):
    if len(results)==0:
        return
    import csv
    with open(fname, 'w') as csvfile:
        fieldnames = results[0].keys()
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(results)

def write_cannot_consume(not_exported):
    if not_exported:
        print('ERROR: -NOT EXPORTED-: %d' % len(not_exported))
        with open('cannot-consume','w+') as fh:
            for x,y in not_exported:
                fh.write(x+'\n')
                traceback.print_exception(*y, limit=2, file=fh)

def delete_files(path,pattern):            
    for pre in path.glob('*.xml'):
        if pre.exists():
            print("Deleting " + str(pre))
            pre.unlink()

def put_args_in_dict(args,*names):
    query = {}
    for name in names:
        arg = getattr(args,name,None)
        if name in ('glob',):
            name = '_' + name
        if isinstance(arg,str):
            query[name] = re.sub('~',' ',arg)
        else:
            query[name] = arg
    return query


class Commands(cmd.Cmd):

    def __init__(self, config):
        self.config = config

    def do_greet(self,line):
        print("Hello "+line)

    def _subcommand(self,label,line,subclass,n=2):
        if len(sys.argv) > n:
            line = ' '.join(sys.argv)
            with Timer(label,line,write_history):
                try:
                    subclass(self.config).onecmd(' '.join(sys.argv[n:]))
                except SystemExit:
                    pass
        else:
            subclass(self.config).cmdloop()

class GRunCleanCommands(Commands):

    def do_titles(self,line):
        """
        performance: 658 subj / sec
        """
        args = get_arguments(line, 'graph', 'fields', 'output', 'separator', 'verbose', 'force', 'reset-cache' )
        for source in args.graph:
            with Timer(label=source) as timer:
                store = load_archive(label=source,config=self.config)
                _els = store.elements(store,reset_cache=args.reset_cache)
                print("Got Elements")
                modified = {}
                with Progress(_els.keys(),100,timer=timer,verbose=True) as progress:
                    for _elk,_elv in _els.items():
                        progress.notify(_elk)
                        title = _elv.get('TITLE',['',])[0]
                        text = _elv.get('TEXT',['',])[0]
                        new = ""
                        if title and text:
                            tit = re.escape(str(title))
                            new = re.sub(tit,'',text,re.I)
                            if len(new) != len(text):
                                modified[_elk] = {
                                    'TEXT': [new,]
                                }
                if args.force:
                    parms = setup_parms(self.config,dbname=source)
                    rep = update_db(source, parms, modified)
                    write_cannot_consume(rep)
                else:
                    export_elements(args, modified, output='titles_'+source+'.csv')


class GRunCommands(Commands):

    def do_clean(self,line):
        self._subcommand("clean",line,GRunCleanCommands,4)

        

class GraphCommands(Commands):

    def do_run(self,line):
        self._subcommand("run",line,GRunCommands,3)


    def do_testprogress(self,line):
        with Timer('test') as timer:
            with Progress(range(100),100,timer=timer,verbose=True) as progress:
                for n in range(100):
                    progress.notify(n)

    def do_dup(self,line):
        """dup - Duplicate graph in memory triplestore

        Has no practical use
        """
        args = get_arguments(line, 'graph','db-out', 'reset-cache','force', 'verbose')
        for source in args.graph:
            with Timer(label=source) as timer:
                store = load_archive(label=source)
                LOGGER.info('COPING source %s' % source)
                new_store = store.duplicate()

    def do_convoy(self,line):
        """convoy - Convoys graphs into new graph (substitute elements)

        
        """
        args = get_arguments(line, 'source','db-out', 'reset-cache','force', 'verbose')
        notify = len(args.source) > 1
        for db in args.source:
            with Timer(label=db,notify=notify):
                parms = setup_parms(self.config,dbname=db)
                convoy_source(parms,db,dbout=args.dbout,force=args.force,
                            reset_cache=args.reset_cache,verbose=args.verbose)

    def do_load(self,line):
        """load source with new method

        >>> arcrad graph load --source arcrad_exagora --db-out out_arcrad_agora --reset-cache --force

        """
        args = get_arguments(line, 'source','db-out', 'reset-cache','force', 'verbose')
        notify = len(args.source) > 1
        for db in args.source:
            with Timer(label=db,notify=notify):
                parms = setup_parms(self.config,dbname=db)
                load_source(parms,db,dbout=args.dbout,force=args.force,
                            reset_cache=args.reset_cache,verbose=args.verbose)

    def do_olddump(self,line):
        """wiki xml - crea i file da importare nel wiki

        arcrad graph dump --path ~/Dropbox/ARCRAD/ARCXML1/  --output output.xml 
        arcrad graph dump --graph out_arcrad_exagora           --output output.xml --limit 2500
        
        """
        args = get_arguments(line, 'graph','output','path','limit','subj', 'glob', 'verbose','force','filter' )
        query = put_args_in_dict(args, 'limit','glob', 'subj', 'force' )
        if args.source:
            for source in args.graph:
                with Timer(label=source,notify=True):
                    store = load_archive(label=source).duplicate()
                    _els = store.elements(store,args.subj,_glob=args.glob,_filter=args.filter)
                    print("Sono stati estratti %d post" % len(_els))
                    exporter = KwCorpusExporter(_els)
                    output = source
                    if args.output is not None:
                        output += "_" + args.output
                    if not re.search('\.xml$',output,re.I):
                        output += ".xml"
                    print("Save in %s" % output)
                    exporter.dump_db_into_xml_files(output, source, **query)
        elif args.path:
            if not args.output:
                args.output = "output.xml"
            path = Path(expandvars(args.path)).expanduser()
            if not path.exists():
                raise ValueError('path not exists')
            exporter.dump_path_into_xml_files(args.output,path, **query)
        else:
            raise ValueError('Mismatch spec for xmlfile')


    def do_dump(self,line):
        """ ddump --source 

        """
        args = get_arguments(line, 'graph','output', 'reset-cache','force', 'verbose', 'limit')
        notify = len(args.graph) > 1
        for db in args.graph:
            with Timer(label=db,notify=notify):
                parms = setup_parms(self.config,dbname=db)
                speed_dump(parms, output=args.output,force=args.force, limit=args.limit,
                           reset_cache=args.reset_cache,verbose=args.verbose)


    def do_fusion(self,line):
        """fusion - fuse multiple graph in a single graph

        >>> arcrad graph fusion --graph out_arcrad_cora out_arcrad_coranew out_arcrad_radicali out_arcrad_oldradicali out_arcrad_mittwoch out_arcrad_exagora --db-out out_arcrad_fusion        

        performance: 24 subj/sec 
                     677131 subj in 28081 sec (~8 hh)
        """
        args = get_arguments(line, 'graph','db-out','output','pred','obj', 'subj', 'create',
                             'fields','separator','glob', 'verbose','force', 'filter' )
        _els = {}
        for source in args.graph:
            store = load_archive(label=source)
            _tels = store.elements(store)
            _els.update( _tels )
        export_elements(args, _els)
        LOGGER.info('Selezionati %d element' % len(_els))

    def do_serialize(self,line):
        """serialize - Write graph in a RDFlib format

        >>> arcrad graph serialize --graph out_arcrad_fusion --output fusion.n3 --format=n3 --enconding=utf-8

        If multiple graph, prepends graph name to the output label.

        performance: 
        """
        args = get_arguments(line, 'format', 'graph', 'encoding', 'output', 'verbose')
        if len(args.graph)==1:
            pre = ["",]
        else:
            pre = [ name + "-" for name in args.graph ]
        for pre,name in zip(pre,args.graph):
            store = load_archive(label="graph_"+name)
            fname = pre + args.output
            store.serialize(destination=fname,format=args.format,encoding=args.encoding)

    def do_sample(self,line):
        """sample - outputs a random fraction of the graph

        >>> arcrad graph sample --graph out_arcrad_exagora           --db-out out_arcrad_sample --percent=20

        
        """
        args = get_arguments(line, 'graph','db-out','output','pred','obj', 'percent', 'subj',
                             'fields','separator','glob', 'verbose','force', 'filter' )
        _els = {}
        for source in args.graph:
            store = load_archive(label=source)
            _tels = store.elements(store,args.subj,_glob=args.glob,_filter=args.filter)
            _els.update( _tels )
        NELS = int((float(args.percent) / 100.0) * len(_els))
        LOGGER.info("Choosing %d (%d%%)elements over %d" % (NELS, args.percent / 100, len(_els)))
        import random
        sample = dict(random.sample(_els.items(),NELS))
        export_elements(args,sample)
                        

    def do_compose(self,line):
        """compose - compose multiple graphs, filtering contents or sampling

        >>> arcrad graph sample --graph out_arcrad_exagora           --db-out out_arcrad_sample --percent=20

        
        """
        args = get_arguments(line, 'graph','db-out','output','pred','obj', 'percent', 'subj',
                             'fields','separator','glob', 'verbose','force', 'filter' )
        _els = {}
        for source in args.graph:
            store = load_archive(label=source)
            _tels = store.elements(store,args.subj,_glob=args.glob,_filter=args.filter)
            _els.update( _tels )
        NELS = int((float(args.percent) / 100.0) * len(_els))
        LOGGER.info("Choosing %d (%d%%)elements over %d" % (NELS, args.percent / 100, len(_els)))
        import random
        sample = dict(random.sample(_els.items(),NELS))
        export_elements(args,sample)

class SourceCommands(Commands):

    def do_import(self,line):
        print("NOT IMPLEMENTED YET: should import mysql dump into db")

    def do_info(self, line):
        args = get_arguments(line, 'also', 'source','output-txt', 'verbose' )
        for db in args.source:
            parms = setup_parms(self.config,dbname=db)
            wdb = MysqlDB(**parms)
            wdb.info(args.also)

    def do_sql(self, line):
        args = get_arguments(line, 'also', 'db', 'output-format', 'output-txt', 'sql', 'verbose' )
        for db in args.db:
            parms = setup_parms(self.config,dbname=db)
            wdb = MysqlDB(**parms)
            results = wdb.sql(' '.join(args.sql))
            export_sql(args.format, args.output, results)

    def do_oldload(self, line):
        """db load DEFS --db-out 

        DEFS: 
        """
        args = get_arguments(line, 'arc', 'db-out', 'verbose', 'force' )
        print("ARGS", args.arc)
        argv = []
        for arg in args.arc:
            if ':' in arg:
                arc, s = arg.split(':')
                if '-' in arg:
                    start, stop = s.split('-')
                else:
                    start = s
                    stop = s
            else:
                arc = arg
                start = 0
                stop = 0
            argv.append((arc,start,stop))
        store = upload_archive(argv,label=args.dbout, force=args.force, create=True)



class DBCommands(Commands):

    def do_stats(self,line):
        args = get_arguments(line, 'db','output-txt', 'verbose' )
        for db in args.db:
            store = load_archive(label=db)
            store.statistics()


    def do_parse(self,line):
        args = get_arguments(line, 'format', 'db-out', 'encoding', 'infile', 'publicid','verbose')
        store = SQLAlchemyTripleStore(label=args.dbout,create=True)
        store.parse(location=args.infile,format=args.format,
                    encoding=args.encoding,publicID=args.publicid)

    def do_sparql(self,line):
        """sparql - query db with bindings
        """
        args = get_arguments(line, 'db','db-out','output','sparql', 'bind', 'ns',
                             'fields','separator','glob', 'verbose','force' )
        args.bind = [ re.sub(r'~', ' ', b) for b in args.bind ]
        binds = dict([ re.split('=',b) for b in args.bind ])
        ns = dict([ re.split('=',b) for b in args.ns ])
        ns['prntp']=str(PRNTP)
        _els = {}
        for db in args.db:
            store = load_archive(label=db)
            query = {
                'query_object': 'SELECT ?s ?p ?o WHERE { ?s ?p ?o . }',
                'ns': ns,
                'initBindings': binds
            }
            if args.sparql:
                query['query_object'] = str(args.sparql)
            _els.update( store.query(store,**query) )
        export_elements(args,_els)
        LOGGER.info('Selezionati %d element' % len(_els))

    def do_digout(self,line):
        """digout - estrae elementi da uno o più db con una lista di subject
        TODO: riunire con list per ottenere un extract
        """
        args = get_arguments(line, 'db','db-out','output','subj-file','pred','obj',
                             'fields','separator','glob', 'verbose','force' )
        query = put_args_in_dict(args, 'glob', 'pred', 'obj' )
        query['filterout'] = args.subj.readlines()
        _els = {}
        for db in args.db:
            store = load_archive(label=db)
            _els.update( store.get_elements(store,**query))
        export_elements(args,_els)
        LOGGER.info('Selezionati %d element' % len(_els))

    def do_list(self,line):
        args = get_arguments(line, 'db','output','subj','pred','obj',
                             'fields','separator','glob', 'verbose','force' )
        query = put_args_in_dict(args, 'glob', 'pred', 'obj' )
        _els = {}
        for db in args.db:
            store = load_archive(label=db)
            _els.update( store.get_elements(store,**query) )
        export_elements(args,_els)
        LOGGER.info('Selezionati %d element' % len(_els))

    def do_export_post(self, line):
        args = get_arguments(line, 'db','delete','path','subj',
                             'glob', 'verbose','force' )
        path = Path(expandvars(args.path)) 
        path.expanduser()
        path.mkdir(parents=True,exist_ok=True)
        if args.delete:
            delete_files(path,"*.xml")
        nep = []
        for db in args.db:
            store = load_archive(label=db)
            _els = store.elements(store,args.subj,_glob=args.glob,_filter=args.filter)
            exporter = KwCorpusExporter(_els)
            exported, not_exported = exporter.dump(path,force=args.force)
            nep.extend(not_exported)
        write_cannot_consume(nep)

    def do_analize(self, line):
        args = get_arguments(line, 'db','howmany','path','subj', 'filter',
                             'glob', 'verbose','force' )
        for db in args.db:
            store = load_archive(label=db)
            _els = store.elements(store,args.subj,_glob=args.glob,_filter=args.filter)
            for key,_el in _els.items():
                (arc,pid) = key.split(':')
                text = _els[args.subj]['TEXT'][0]
                soup = BeautifulSoup(text,"lxml")
                text = soup.prettify()
                consumer = KwConsumer(arc,pid,_el['TEXT'][0],**_el)
                consumer.consume()

    def do_show(self, line):
        """show - permette di vedere le informazioni relativi ad un o più post

        >>> arcrad show --db ARCRAD_exag --subj=EXAG:47044 
        >>> arcrad show --db ARCRAD_exag --subj=EXAG:47044  --keys DOCID KEY
        >>> arcrad show --db ARCRAD_exag --subj=EXAG:47044  --keys DOCID KEY --output output.csv

        """
        args = get_arguments(line, 'db','db-out', 'output', 'howmany','path', 'subj', \
                             'filter','glob', 'verbose','force', 'keys' )
        for db in args.db:
            store = load_archive(label=db)
            _els = store.elements(store,args.subj,_glob=args.glob,_filter=args.filter)
            for key,_el in _els.items():
                (arc,pid) = key.split(':')
                text = _els[args.subj]['TEXT'][0]
                soup = BeautifulSoup(text,"lxml")
                text = soup.prettify()
                consumer = KwConsumer(arc,pid,_el['TEXT'][0],**_el)
                consumer.consume()
                info = consumer.info
                if args.keys:
                    info = { key:value for key, value in info.items() if key in args.keys }
                info = { args.subj : info }
                export_elements(args,info)

class WikiCommands(Commands):

    def __init__(self, config):
        self.config = config
        self.parms = {
            'host': config.get('WIKIDB','host',fallback='127.0.0.1'),
            'user': config.get('WIKIDB','user',fallback='root'),
            'passwd':  config.get('WIKIDB','passwd',fallback='root'),
            'dbname':  config.get('WIKIDB','dbname',fallback='wk_arcrad_t1'),
            'port':  config.get('WIKIDB','port', fallback=8889 ) ,
        }

    def do_restore(self, line):
        """arcrad wiki restore --config ~/Dropbox/code/archivio-radicale/wikis/w/LocalSettings.php --file out_arcrad_sample.xml --verbose
        """
        args = get_arguments(line, 'config','file','verbose' )
        self.do_reset(line)
        self.do_upload(line)

    def do_upload(self, line):        
        args = get_arguments(line, 'config', 'file', 'xml-files', 'verbose' )
        import subprocess
        with FilePattern(fname=args.file) as files:
            with Timer("upload",args.file,notify=True) as timer:
                with Progress(files,100,timer,verbose=True) as progress:
                    for fname in files:
                        LOGGER.info('Importing file %s' % str(fname))
                        self._php_execute('maintenance/importDump.php','--report','--globals',str(fname))
                        progress.notify(fname,verbose=True)
        for fname in args.xmlfiles:
            LOGGER.info('Importing also file %s' % str(fname))
            self._php_execute('maintenance/importDump.php',str(fname))
        self._php_execute('maintenance/rebuildrecentchanges.php')

    def do_reset(self, line):
        args = get_arguments(line,  'config', 'file', 'xml-files', 'verbose' )
        mysql_drop_all_tables(**self.parms)
        mysql_send_file("resetdb.sql", **self.parms)
        if args.xmlfiles:
            self.do_upload(line)
            

    def _php_execute(self, script, *args):
        import subprocess
        php_home = self.config.get('PHP','php',fallback='php')
        wiki_home = self.config.get('WIKI','root',fallback='/Applications/MAMP/htdocs/wiki')
        wiki_conf = Path(wiki_home) / 'LocalSettings.php'
        php_script = Path(wiki_home) / script 
        sargs = ( php_home , str(php_script.expanduser()), '--conf', str(wiki_conf.expanduser()), *args)
        print(' '.join(sargs))
        ret = subprocess.run(sargs,stderr=subprocess.STDOUT,shell=False,universal_newlines=True)
        LOGGER.info("PHP script %s RET=%s" % (script, ret.stdout))
            

class ArcradCommand(Commands):
    """Accepts commands via the normal interactive prompt or on the command line."""

    def do_wiki(self, line): 
        self._subcommand("wiki",line,WikiCommands)

    def do_db(self, line):
        self._subcommand("db",line,DBCommands)

    def do_graph(self, line):
        self._subcommand("graph",line,GraphCommands)

    def do_source(self, line):
        self._subcommand("source",line,SourceCommands)

    def do_shell(self,line):
        args = get_arguments(line, 'cmd', 'verbose' )
        cmds = dict(
            map(lambda x: (x[0],x[1]()),
                filter( lambda obj: isinstance(obj[1],type) and \
                        issubclass(obj[1], Command) and \
                        obj[0] != 'Command' , globals().items() )))
        namespace = {'recon': Recognizers, 'Corpus': Corpus}
        namespace.update(cmds)
        IPython.start_ipython(argv=['--simple-prompt','-i'],
                              user_ns=namespace)

    def do_version(self, line):
        from . import __version__
        print(__version__)

    def do_EOF(self, line):
        return True


def write_history(self, type, value, traceback):
    """Timer func    
    Here self is Timer object
    """
    with Path(expandvars(HISTORYFILE)).expanduser().open('a+') as fh:
            fh.write(str(datetime.now()))
            if self.name:
                fh.write(' [%s]' % self.name)
            fh.write(' : %s\n' % (time.time() - self.tstart))

def main():
    import sys
    import configparser, os
    config = configparser.ConfigParser()
    config.read(['pyarcrad.cfg', 
                 os.path.expanduser('~/.pyarcrad.cfg')],
                encoding='utf-8')
    elapsed = 0.0
    if len(sys.argv) > 1:
        line = ' '.join(sys.argv)
        with Timer("main",line,notify=True) as timer:
            try:
                ArcradCommand(config).onecmd(' '.join(sys.argv[1:]))
            except SystemExit:
                pass
            elapsed = timer.elapsed
    else:
        ArcradCommand(config).cmdloop()
