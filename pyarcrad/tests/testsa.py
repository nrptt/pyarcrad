import re
import unittest
from collections import defaultdict
from hashlib import sha224
from pprint import pformat, pprint

from pyarcrad.exporter import KwCorpusExporter
from pyarcrad.graph import SQLAlchemyTripleStore, load_archive, upload_archive
from pyarcrad.utils import PRNTP
from rdflib import Namespace, URIRef


class TestSQLAlchemyTripleStore(unittest.TestCase):
    def setUp(self):
        self.store = load_archive()


    def test_query1(self):
        ns = dict(prntp=PRNTP)
        xid = URIRef('http://data.xed.it/prntt/2016/v1.0/DOCID')
        _els = {}
        for row in self.store.graph.query(
                 """SELECT ?s ?p ?o WHERE { ?s ?p ?o }""",
                 initNs=ns,
                 DEBUG=True):
            _s = row[0][35:]
            _p = row[1][35:]
            _o = row[2]
            if _s not in _els:
                _els[_s] = defaultdict(list)
            _els[_s][_p].append(_o)
        print(len(_els))

    def test_dump(self):
        ns = dict(prntp=PRNTP)
        xid = URIRef('http://data.xed.it/prntt/2016/v1.0/DOCID')
        xid = URIRef('http://data.xed.it/prntt/2016/v1.0/EXA:15647')
        xid = URIRef('http://data.xed.it/prntt/2016/v1.0/MITT:61147')
        _els = {}
        for row in self.store.graph.query(
                 """SELECT ?s ?p ?o WHERE { ?s ?p ?o }""",
                 initNs=ns,
                 # initBindings={'s' : xid},
                 DEBUG=True):
            _s = row[0][35:]
            _p = row[1][35:]
            _o = row[2]
            if _s not in _els:
                _els[_s] = defaultdict(list)
            _els[_s][_p].append(str(_o))
        exporter = KwCorpusExporter(_els)
        print('exporting corpus')
        exporter.dump('/tmp/corpus')


if __name__ == '__main__':
    unittest.main()
