import mysql.connector
import mysql.connector.cursor as mc
from mysql.connector.errors import *


class config:
    USER='root'
    PASS='root'
    HOST='localhost'
    PORT='8889'
    MYDB='arcrad_cd'


class Cursor(object):

    def __init__(self,
                 host=config.HOST, user=config.USER,
                 passwd=config.PASS, dbname=config.MYDB,
                 port=config.PORT,
                 driver=mysql.connector,
                 ):

        self.host = host
        self.user = user
        self.port = port
        self.passwd = passwd
        self.dbname = dbname
        self.driver = driver
        try:
            self.connection = self.driver.connect(
                user=user, passwd=passwd,
                db=dbname, port=port, host=host)
        except DatabaseError as exc:
            print(exc)
            raise
        except:
            raise
        self.cursor = self.connection.cursor(dictionary=True)

    def __iter__(self):
        for item in self.cursor:
            yield item

    def __enter__(self):
        return self.cursor

    def __exit__(self, ext_type, exc_value, traceback):
        self.cursor.close()
        if isinstance(exc_value, Exception):
            self.connection.rollback()
        else:
            self.connection.commit()
        self.connection.close()

    def execute(self, sql, *args):
        self.cursor(sql, *args)


for i in range(1,1000):
     with Cursor() as cursor:
        cursor.execute("SELECT %d AS V" % i)
        c = cursor.fetchone()
        print(i,c)
        cursor.close()
