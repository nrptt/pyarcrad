import re
import unittest
from hashlib import sha224
from pprint import pformat, pprint

from pyarcrad import (ARCD_DB, ARDB, EXAG, EXAG_DB, MITT_DB, MWDB1_DB, RADIT_DB, ARCD_Consumer,
                      ARCD_Exporter, Archive, ArchivioRadicaleP, Corpus, EXAG_Consumer,
                      EXAG_Exporter, PostInfo, Recognizers, add_triples_to_graph, triples_to_dict)
from pyarcrad.graph import SQLAlchemyTripleStore, load_archive, upload_archive
from pyarcrad.utils import PRNTP
from rdflib import Namespace, URIRef


# Containers Tests

class Test0ARCD_DB(unittest.TestCase):

    def test_get(self):
        container = ARCD_DB()
        pid = 1
        doc = container.get(pid)
        self.assertEqual(doc['PID'], pid)
        self.assertEqual(doc['AUTHOR'], "SALVEMINI GAETANO")

    def test_ids1(self):
        container = ARCD_DB()
        ids = container.ids()
        self.assertEqual(len(ids), 23378)

    def test_ids2(self):
        container = ARCD_DB()
        ids = container.ids(23301)
        self.assertEqual(len(ids), 78)

    def test_ids3(self):
        container = ARCD_DB()
        ids = container.ids(-100)
        self.assertEqual(len(ids), 100)

    def test_gettriples(self):
        container = ARCD_DB()
        pid = 1
        triples = container.get_triples(pid)
        aDict = triples_to_dict(triples)
        self.assertEqual(aDict['ARCD:1']['AUTHOR'], 'SALVEMINI GAETANO')


class Test0MWDB1_DB(unittest.TestCase):

    def test_get(self):
        container = MWDB1_DB()
        pid = container.ids(-1)[0]
        doc = container.get(pid)
        self.assertEqual(doc['PID'], pid)
        self.assertEqual(doc['TITLE'], "il Partito Radicale Nonviolento, transnazionale e transpartito")
        pprint(doc)

    def test_ids0(self):
        container = MWDB1_DB()
        ids = container.ids()
        self.assertEqual(ids[0], 31)

    def test_ids1(self):
        container = MWDB1_DB()
        ids = container.ids()
        self.assertEqual(len(ids), 117981)

    def test_ids2(self):
        container = MWDB1_DB()
        ids = container.ids(23300)
        self.assertEqual(len(ids), 112993)

    def test_ids3(self):
        container = MWDB1_DB()
        ids = container.ids(-100)
        self.assertEqual(len(ids), 100)

    def test_gettriples(self):
        container = MWDB1_DB()
        pid = 7677 # container.ids(100)[0]
        triples = container.get_triples(pid)
        pprint(triples_to_dict(triples))
        

class Test0RADIT_DB(unittest.TestCase):

    def test_get(self):
        container = RADIT_DB()
        pid = container.ids(-1)[0]
        doc = container.get(pid)
        self.assertEqual(doc['PID'], pid)
        self.assertEqual(doc['TITLE'], "il Partito Radicale Nonviolento, transnazionale e transpartito")
        pprint(doc)

    def test_ids0(self):
        container = RADIT_DB()
        ids = container.ids()
        self.assertEqual(ids[0], 31)

    def test_ids1(self):
        container = RADIT_DB()
        ids = container.ids()
        self.assertEqual(len(ids), 117981)

    def test_ids2(self):
        container = RADIT_DB()
        ids = container.ids(23300)
        self.assertEqual(len(ids), 112993)

    def test_ids3(self):
        container = RADIT_DB()
        ids = container.ids(-100)
        self.assertEqual(len(ids), 100)

    def test_gettriples(self):
        container = RADIT_DB()
        pid = 49 # container.ids(100)[0]
        triples = container.get_triples(pid)
        pprint(triples_to_dict(triples))
        

class TestPostInfo(unittest.TestCase):
    
    def test_dict1(self):
        d = PostInfo()
        self.assertEqual(d,{})

    def test_dict2(self):
        d = PostInfo(a=1,b=2,c=3)
        self.assertEqual(len(d),3)
        self.assertEqual(len(d.data),3)


class Test0MITT_DB(unittest.TestCase):

    def test_get(self):
        container = MITT_DB()
        pid = 445529
        doc = container.get(pid)
        self.assertEqual(doc['PID'], pid)
        self.assertEqual(len(doc['TEXT']), 6288)

    def test_ids1(self):
        container = MITT_DB()
        ids = container.ids()
        self.assertEqual(len(ids), 310275)

    def test_ids00(self):
        container = MITT_DB()
        ids = container.ids(0)
        self.assertEqual(ids[0], 34867)

    def test_ids01(self):
        container = MITT_DB()
        ids = container.ids(0)
        self.assertEqual(ids[-1], 474388)

    def test_ids2(self):
        container = MITT_DB()
        ids = container.ids(474378)
        self.assertEqual(len(ids), 10)

    def test_ids3(self):
        container = MITT_DB()
        ids = container.ids(-100)
        self.assertEqual(len(ids), 100)

    def test_gettriples(self):
        container = MITT_DB()
        pid = 36458 # container.ids(-1)[0]
        triples = container.get_triples(pid)
        dtriples = triples_to_dict(triples)
        self.assertEqual(len(dtriples['MITT:36458']['KEY']), 8)

# Exporters Tests


class Test1ARCD_Exporter(unittest.TestCase):
    DB = ARCD_DB
    Exporter = ARCD_Exporter

    def setUp(self):
        pass

    def test_10compose_from_dict(self):
        exporter = self.Exporter([], test='foo', bar='foobar')
        composed = exporter.compose_from_dict(exporter.info)
        self.assertEqual('| bar=foobar\n| test=foo', composed)

    def test_20compose(self):
        exporter = self.Exporter([], test='foo', bar='foobar')
        composed = exporter.compose('test', exporter.info)
        composed = re.sub(r'\| LDATE = .+\n', '', composed, re.M)
        self.assertEqual('{{test\n| bar=foobar\n| test=foo\n}}', composed)

    def test_30output(self):
        exporter = self.Exporter([], id='ID1', TITLE='foo', abstract='foobar', meta={
                                 'foo': 'bar', 'DOCID': 'ID1'}, SOURCE='0', ARCHIVIO='AR',
                                 LINGUA='IT', p=['testo1', 'testo2'])
        composed = exporter.output()
        composed = re.sub(r'\| LDATE = .+\n', '', composed, re.M)
        m = sha224(composed.encode('utf-8')).hexdigest()
        self.assertEqual('24c3be931004f0a96d2d31c238fe120af9b4da02b785d7a6884f134d', m)

    def test_40from_DB(self):
        container = self.DB()
        pid = 1
        doc = container.get(pid)
        exporter = self.Exporter([], **doc)
        composed = exporter.output()
        composed = re.sub(r'\| LDATE = .+\n', '', composed, re.M)
        m = sha224(composed.encode('utf-8')).hexdigest()
        self.assertEqual('965467947036bb0a3a528846782b980a2353c9c7ec44b8d5395261cd', m)

    def test_infobox(self):
        container = self.DB()
        pid = 1
        doc = container.get(pid)
        exporter = self.Exporter(container.get_triples(pid), **doc)
        infobox = dict(exporter.infobox())
        self.assertEqual(infobox['ARCLABEL'], 'ARCPR')


class Test1EXAG_Exporter(unittest.TestCase):
    DB = EXAG_DB
    Exporter = EXAG_Exporter

    def setUp(self):
        pass

    def test_10compose_from_dict(self):
        exporter = self.Exporter([], test='foo', bar='foobar')
        composed = exporter.compose_from_dict(exporter.info)
        self.assertEqual('| bar=foobar\n| test=foo', composed)

    def test_20compose(self):
        exporter = self.Exporter([], test='foo', bar='foobar')
        composed = exporter.compose('test', exporter.info)
        composed = re.sub(r'\| LDATE = .+\n', '', composed, re.M)
        self.assertEqual('{{test\n| bar=foobar\n| test=foo\n}}', composed)

    def test_30output(self):
        exporter = self.Exporter([], id='ID1', TITLE='foo', abstract='foobar', meta={
                                 'foo': 'bar', 'DOCID': 'ID1'}, SOURCE='0', ARCHIVIO='AR', LINGUA='IT', p=['testo1', 'testo2'])
        composed = exporter.output()
        composed = re.sub(r'\| LDATE = .+\n', '', composed, re.M)
        m = sha224(composed.encode('utf-8')).hexdigest()
        self.assertEqual('b222a174fd6f8008b30bc32d33150da76e9ecc15e0d8ed2844aafc52', m)

    def test_40from_DB(self):
        container = self.DB()
        pid = 15647
        doc = container.get(pid)
        exporter = self.Exporter([], **doc)
        composed = exporter.output()
        composed = re.sub(r'\| LDATE = .+\n', '', composed, re.M)
        m = sha224(composed.encode('utf-8')).hexdigest()
        self.assertEqual('ffff780fcde6fb2794ac38f45ce6a7cf8cc28fee6a5ad5ac8b1c5f96', m)


# Consumer Tests

class ConsumerTest(unittest.TestCase):
    Container = None
    Consumer = None
    PID = 1
    RETURN_1 = None
    RETURN_2 = None

    def setUp(self):
        if not self.Container:
            raise OSError
        self.container = self.Container()
        self.info = self.container.get(self.PID)
        if self.info:
            self.__dict__.update(self.info)


class Test2ARCD_Consumer(ConsumerTest):
    Container = ARCD_DB
    Consumer = ARCD_Consumer
    PID = 1

    def test_return1(self):
        self.assertEqual(self.AUTHOR, "SALVEMINI GAETANO")

    def test_consume1(self):
        consumer = self.Consumer(self.PID, self.TEXT)
        consumer.consume()
        m = sha224(pformat(
            consumer.info).encode('utf-8')).hexdigest()
        self.assertEqual(m, 
                         'cd71451fde8427fff08f241c843e8c32770ecb2891bfa6206b0a0abd')

    def test_gettriples(self):
        consumer = self.Consumer(self.PID, self.TEXT,**self.info)
        consumer.consume()
        triples = self.container.get_triples(self.PID)
        triples.extend(consumer.get_triples())
        aDict = triples_to_dict(triples)
        self.assertEqual(aDict['ARCD:1']['AUTHOR'], 'SALVEMINI GAETANO')


class Test2EXAG_Consumer(ConsumerTest):
    Container = EXAG_DB
    Consumer = EXAG_Consumer
    PID = 15647

    def test_return1(self):
        self.assertEqual(self.AUTHOR, "Di Robilant Andrea")

    def test_consume1(self):
        consumer = self.Consumer(self.PID, self.TEXT, **self.info)
        consumer.consume()
        m = sha224(pformat(consumer.info).encode('utf-8')).hexdigest()
        self.assertEqual(m,
                         'a680beb46acc67db533515ca7313f9b8ed2af33ee01ef6264c3d4a21')

    def test_gettriples(self):
        consumer = self.Consumer(self.PID, self.TEXT,**self.info)
        consumer.consume()
        triples = self.container.get_triples(self.PID)
        triples.extend(consumer.get_triples())
        aDict = triples_to_dict(triples)
        self.assertEqual(aDict['EXA:15647']['AUTHOR'], 'Di Robilant Andrea')

# Archives Tests


class TestARDB(unittest.TestCase):

    def test_get(self):
        fonte = ARDB()
        pid = 1
        fonte.get(pid)
        self.assertEqual(fonte.docid, 'ARCD:1')
        self.assertEqual(fonte.info['meta']['AUTHOR'], 
                         'SALVEMINI GAETANO')
        adict = triples_to_dict(fonte.triples)
        self.assertEqual(adict['ARCD:1']['AUTHOR'], 
                         'SALVEMINI GAETANO')
        self.assertEqual(fonte.info['meta']['AUTHOR'], 
                         'SALVEMINI GAETANO')
        self.assertEqual(fonte.dtriples['ARCD:1']['AUTHOR'], 
                         'SALVEMINI GAETANO')

    def test_graph(self):
        fonte = ARDB()
        pid = 1
        fonte.get(pid)
        exporter = fonte.Exporter(fonte.triples, **fonte.info)

    def test_infobox(self):
        fonte = ARDB()
        pid = 1
        fonte.get(pid)
        infobox = fonte.exporter.infobox()
        dinfobox = dict(infobox)
        self.assertEqual(dinfobox['ARCLABEL'], 'ARCPR')


class TestEXAG(unittest.TestCase):

    def test_get(self):
        fonte = EXAG()
        fonte.get(15647)
        


class TestRecon(unittest.TestCase):

    def test_recognizers(self):
        for fonte, pid in (("ARDB", 1), ('EXAG', 15647)):
            fonte = Recognizers[fonte]()
            fonte.get(pid)

# ArchiveCorpusReader


class TestArchiveCorpusReader(unittest.TestCase):

    def test_corpus(self):
        for fonte, pid, risultato in (
                ("ARDB", 1, 'SALVEMINI GAETANO'), 
                ('EXAG', 15647, 'Di Robilant Andrea')):
            corpus = Corpus(fonte, start=pid, stop=pid)
            corpus.load()
            self.assertEqual(corpus[pid].info['meta']['AUTHOR'],
                             risultato)

    def test_corpus2(self):
        for fonte, pid, risultato in (
                ("ARDB", 1, 'PARTITO RADICALE'), 
                ('EXAG', 15647, 'Bonino Emma')):
            corpus = Corpus(fonte, start=pid, stop=pid+3)
            corpus.load()
            self.assertEqual(corpus[pid+3].info['meta']['AUTHOR'],
                             risultato)

    def test_dump(self):
        for fonte, pid, risultato in (
                ("ARDB", 1, '1ce609b1924fd12b7c493fc4029d4dc1bc526d17487b16398955ddf4'), 
                ('EXAG', 15647, 'ca0e2eae91311f52819b65ec360fc746239e251296633b4ce4fe2fff')):
            corpus = Corpus(fonte, start=pid, stop=pid+3)
            corpus.load()
            dump = corpus.dumps()
            dump = re.sub(r'\| LDATE = .+\n', '', dump, flags=re.M)
            m = sha224(dump.encode('utf-8')).hexdigest()
            self.assertEqual(m,risultato)
    

class TestArchive(unittest.TestCase):

    def test_archive(self):
        with Archive(ArchivioRadicaleP) as archive:
            pass
        

class TestSQLAlchemyTS(unittest.TestCase):

    def test_store1(self):
        fonte = ARDB.Container()
        pid = 1
        fonte.get(pid)
        store = SQLAlchemyTripleStore()
        graph = store.graph
        graph = add_triples_to_graph(graph,'test',fonte.triples)


class TestUpload(unittest.TestCase):
    def setUp(self):
        self.store = upload_archive(ArchivioRadicaleP)

    def tearDown(self):
        # self.store.destroy()
        pass

    def test_query1(self):
        ns = dict(prntp=PRNTP)
        xid = URIRef('http://data.xed.it/prntt/2016/v1.0/ARCD:83')
        for row in self.store.graph.query(
                 """SELECT ?p ?pred ?o WHERE { ?p ?pred ?o }""",
                 initNs=ns,
                 initBindings={'p' : xid},
                 DEBUG=True):
             print(row)
        # # store.clear()




class TestSQLAlchemyTripleStore(unittest.TestCase):
    def setUp(self):
        self.store = load_archive()


    def test_query1(self):
        ns = dict(prntp=PRNTP)
        xid = URIRef('http://data.xed.it/prntt/2016/v1.0/ARCD:83')
        for row in self.store.graph.query(
                 """SELECT ?p ?pred ?o WHERE { ?p ?pred ?o }""",
                 initNs=ns,
                 initBindings={'p' : xid},
                 DEBUG=True):
             print(row)
        

if __name__ == '__main__':
    unittest.main()
