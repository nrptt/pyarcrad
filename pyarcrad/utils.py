import math
import re
import sys
import time
from collections import UserDict
from pathlib import Path
from pprint import pprint
from urllib.parse import urljoin

from rdflib import Graph, Literal, Namespace, URIRef
from six import string_types

from . import LOGGER
from .config import DOMAIN, VERSION


def dict_to_triples(_dict):
    triples = []
    for _k1, _els in _dict.items():
        for _k2, _v in _els.items():
            for _val in _v:
                triples.append( (_k1,_k2, _val ))
    return triples

def triples_to_dict(triples):
    aDict = {}
    if not triples:
        return
    for subj, pred, obj in triples:
        if subj not in aDict:
            aDict[subj] = {}
        if pred in aDict[subj]:
            if not isinstance(aDict[subj][pred], list):
                oldv = aDict[subj][pred]
                if oldv != obj:
                    aDict[subj][pred] = [oldv, obj]
            else:
                aDict[subj][pred].append(obj)
        else:
            aDict[subj][pred] = obj
    return aDict

def triples_to_graph(app,triples):
    g = Graph()
    PATH = 'posts'
    for s, p, o in triples:
        node = URIRef(
            urljoin(DOMAIN, "%s/%s/%s/%s" % (app, VERSION, PATH, s)))
        g.add((node, Literal(p), Literal(o)))
    return g

def divide(l,n):
    L = len(l)
    N = int(L / n)
    return chunks(l,N)

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def split_list_and_take_first(l,n):
    xchunks = list(divide(l,n))
    firsts = []
    for chunk in xchunks:
        firsts.append(chunk[0])
    return firsts

def add_triples_to_graph(graph, app, triples, func=lambda x,y: x.add(y), verbose=False):
    # from rdflib.namespace import DC, FOAF
    from pyarcrad.utils import PRNTT, PRNTP
    tlist = list(set(triples))
    with Timer('add_triples') as timer:
        printed = False
        with Progress(tlist,100,timer=timer,verbose=True) as progress:
            for n,(s, p, o) in enumerate(tlist):
                progress.notify((s,p,o),verbose=True)
                node = URIRef(s)
                if not re.match(r'^http://',s,re.I):
                    node = PRNTT.term(s)
                pred = URIRef(p)
                if not re.match(r'^http://',p,re.I):
                    pred = PRNTP.term(p)
                triple = (node, pred, Literal(str(o)))
                if triple:
                    func(graph,triple)
    return graph

def set_triples_to_graph(graph, app, triples, func=lambda x,y: x.set(y), verbose=False):
    return add_triples_to_graph(graph, app, triples, func, verbose)

PRNTT = Namespace('http://data.xed.it/prntt/2016/v1.0/')
PRNTP = Namespace('http://data.xed.it/prntt/2016/v1.0/')

class PostInfo(UserDict):
    
    def __init__(self, *args, **kwargs):
        self.data = {}
        UserDict.__init__(self, *args, **kwargs)

    def xget(self, key, default=None):
        value = self.get(key,default)
        # if len(value)>1:
        #     value = ','.join(value)
        # else:
        #     value = value[0]
        value = ','.join(value)
        return value

    def setupMeta(self):
        """
        meta
             PID
             SOURCE
             DOCID
             LANG
             ARCID
             ARCNAME
        """
        self.cpMeta('meta',
                    ( 'PID',
                      ('SOURCE', '_SOURCE' ),
                      'LANG',
                      'ARCID',
                      'ARCNAME'
                    ))
        if 'meta' in self.data and '_SOURCE' in self.data and 'PID' in self.data:
            self.data['meta']['DOCID'] = "%s:%s" % (self.data['_SOURCE'], self.data['PID'])


    
    def cpMeta(self, tokey, fromkeys):
        if tokey not in self.data:
            self.data[tokey] = {}
        for defn in fromkeys:
            if isinstance(defn,string_types):
                tkey = defn
                fkey = lambda doc: doc[defn]
                xkey = defn
            elif isinstance(defn,tuple):
                tkey = defn[0]                
                if isinstance(defn[1], string_types):
                    fkey = lambda doc: doc[defn[1]]
                    xkey = defn[1]
                else:
                    raise OSError
            else:
                raise OSError
            if xkey in self.data:
                self.data[tokey][tkey] = fkey(self.data)
        if self.data[tokey] == {}:
            del self.data[tokey]
        return self.data


class Progress(object):
    def __init__(self, elems, npieces=10, timer=None, verbose=False, pre=""):
        self.elems = list(elems)
        if len(self.elems) < npieces:
            npieces = len(self.elems)
        self.percent = 100.0 / npieces
        self.range = self.elems
        self.partition = split_list_and_take_first(self.elems,npieces)
        self.length = len(self.elems)
        self.timer = timer
        self.fieldlen = len(str(npieces))
        self.verbose = verbose
        self.pre = pre

    @property
    def elapsed(self):
        if self.timer:
            return self.timer.elapsed
        
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        return 

    def eta(self, pid):
        return 0
        # return float((float(self.elapsed) / float((pid * len(self.partition) + 1.0 ))) / 60.0)

    def notify(self, id_el, verbose=True):
        prin = False
        if verbose and id_el in self.partition:
            pid = self.partition.index(id_el)
            pc = pid * self.percent
            prin = True
            endl = "\n"
        if prin:
            if self.timer:
                print("%s%*.0f%% (%.0f/%.0f)..." % (self.pre,self.fieldlen,pc,self.elapsed,self.eta(pid)),end=endl, flush=True)
            else:
                print("%s%*.0f%% ..." % (self.pre,self.fieldlen,pc),end="", flush=True)


class Timer(object):
    level = 0

    def __init__(self, label, name=None, exit_func=None, history=True, notify=False):
        self.label = label
        self.want_notify = notify
        self.name = name if name else label
        self.exit_func = exit_func
        self.history = history
        self.filename = Path('~/.pyarcrad.time.%s' % self.label).expanduser()

    def notify(self):
        import pync
        pync.Notifier.notify('Work [%s] done\n%s\nin %3.2f sec' % (self.label,' '.join(sys.argv[1:]),self.elapsed))


    def average_from_history(self):
        values = list(self.load_history())
        from statistics import mean, stdev        
        if len(values)>2:
            return mean(values), stdev(values)
        else:
            return 0.0, 0.0

    def load_history(self):    
        if self.filename.exists():
            with self.filename.open('r') as fh:
                rl = fh.readlines()
            return filter(lambda x: x>0.05,map(float,rl))
        return 0.0,0.0
    
    def save_history(self):
        with self.filename.open('a') as fh:
            fh.write("%3.2f\n" % self.elapsed)

    def __enter__(self):
        self.level += 1
        avg = ""
        if self.history:
            avg = " AVG=%3.2f sTD=%3.2f" % self.average_from_history()
        LOGGER.info('%sTimer started for [%s]%s' % (' '*self.level, self.name, avg))
        self.tstart = time.time()
        return self

    def __exit__(self, type, value, traceback):        
        self.save_history()
        if self.want_notify:
            self.notify()
        if self.exit_func:
            self.exit_func(self, type, value, traceback)
        LOGGER.info('%s[%s] in %f sec' % (' '*self.level,
            self.name if self.name else "?", self.elapsed))
        self.level -= 1



    @property
    def elapsed(self):
        return (time.time() - self.tstart)

class FilePattern(object):
    def __init__(self, path=".", glob=None, fname=None):
        self.path = Path(path)
        self.glob = glob
        parent = self.path.parent
        if fname and not glob:
            name = Path(fname)
            suffix = name.suffix
            stem = name.stem
            self.glob = stem + "*" + suffix        
        self.files = list(parent.glob(self.glob))

    def __enter__(self):
        return self.files

    def __exit__(self, type, value, traceback):
        pass


def setup_parms(config,**kw):
    if config:
        ret = {
            'host': config.get('WIKIDB','host',fallback='127.0.0.1'),
            'user': config.get('WIKIDB','user',fallback='root'),
            'passwd':  config.get('WIKIDB','passwd',fallback='root'),
            'dbname':  config.get('WIKIDB','dbname',fallback='wk_arcrad_t1'),
            'port':  config.get('WIKIDB','port', fallback=8889 ) ,
        }
        ret.update(kw)
        return ret
