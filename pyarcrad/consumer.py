
import html
import json
import logging
import re
from pprint import pformat

from bs4 import BeautifulSoup
from lxml import etree

from .substitutions import LOCAL_SUBST

LOGGER = logging.getLogger(__name__)


def escape(x):
    PASS_TAGS = ('html', 'head', 'body', 'blockquote', 'i', 'img', 'snip', 'font', 
                 'strong', 'br', 'h1', 'h2', 'h3','a', 'ol', 'ul', 'object', 'pre',
                 'em', 'b', 'center', 'div', 'h4', 'hr', 'li', 'll', 'param', 
                 'script', 'span', 'table', 'td ', 'th ', 'tr', 'u')
    tag = x.group(1)
    if tag.lower() in PASS_TAGS:
        return "<" + tag + ">"
    return html.escape("<" + tag + ">")

DETAG = (r'<([^>]+)>', escape, re.I)

def hescape(x):
    return html.escape(x.group(0))

class Consumer(object):
    """Il Consumer legge l'articolo e trova le variabili informative che registra in info

    Le chiavi di info sono:

    p : contiene le linee del testo
    SOURCE : contiene l'identificativo della fonte documentale
    meta : contiene un dict con le informazioni che saranno riportate nella pagina
    abstract : sommario del testo

    in meta le chiavi importanti sono:

    PID : identificazione del documento (TODO: rinominare in DOCID)
    LINGUA : Lingua del documento
    SOURCE : identificativo della fonte documentale
    CHIAVI : keywords/categorie (list)
    RIFERIMENTO : 
    trad_<XX> : dove XX è il codice della lingua - identificativo della traduzione

    """
    SUBST_ALL = (
        (r'.', r'<(/?)(BR|HR)>', u'<\\1P>', re.I),
    )
    SUBST = {}
    DEBUG_FILE = False
    PASS_TAGS = ('html', 'head', 'body', 'blockquote', 'i', 'img', 'snip', 'font', 
                 'strong', 'br', 'h1', 'h2', 'h3','a', 'ol', 'ul', 'object', 'pre',
                 'em', 'b', 'center', 'div', 'h4', 'hr', 'li', 'll', 'param', 
                 'script', 'span', 'table', 'td ', 'th ', 'tr', 'u')

    def __init__(self, pid, html_document, *args, **kwargs):
        self.pid = pid
        self.document = html_document
        self.info = {
            'p': [],
            'SOURCE': self.Archivio,
            'DOCID': pid,
            'meta': {'PID':  pid},
        }
        self.info.update(**kwargs)
        self.abstract = None
        self.rewrite()

    def rewrite(self):
        if self.SUBST_ALL:
            for target, regexp, subst, flags in self.SUBST_ALL:
                if re.match(target, self.document, re.I):
                    self.document = re.sub(regexp, subst, self.document, flags=flags)
        if self.SUBST:
            if self.pid in self.SUBST:
                for regexp, subst, flags in self.SUBST[self.pid]:
                    self.document = re.sub(regexp, subst, self.document, flags=flags)

    def consume(self):
        if self.DEBUG_FILE:
            self.save_page()
        #
        event_types = ("start",)
        hparser = etree.HTMLPullParser(event_types)
        events = hparser.read_events()
        hparser.feed(self.document)
        self.consume_events(events)
        #
        self.info['empty'] = False
        if self.info['p']:
            self.identify()
        else:
            self.info['empty'] = True
        if self.DEBUG_FILE:
            self.save_info()

    def identify(self):
        # LOGGER.debug('CON:identify')
        self.info['meta']['DOCID'] = "%s:%d" % (self.Archivio, self.info['meta']['PID'])
        self.info['meta']['VER'] = '0'

    def save_page(self):
        fname = 'doc.html'
        LOGGER.info('Saving page in %s' % fname)
        with open(fname, 'wb') as f:
            f.write(self.document.encode('utf-8'))

    def save_info(self):
        fname = 'doc'
        LOGGER.info('Saving page in %s' % fname)
        self.info['DATE'] = self.info['DATE'].isoformat()
        with open(fname + ".json", 'w') as f:
            json.dump(self.info, f, indent=True, skipkeys=True)
            f.write('-' * 50)
            f.write(pformat(self.info))

    def consume_start_p(self, obj):
        text = obj.text
        if text:
            if re.match(r'^$', text):
                pass
            else:
                self.info['p'].append(text)

    def consume_events(self, events):
        for action, obj in events:
            if action in ('start', 'end'):
                # print (action, obj.tag)
                method = "consume_%s_%s" % (action, obj.tag)
                if hasattr(self, method):
                    method = getattr(self, method)
                    method(obj)
                elif action in ('end'):
                    pass
                elif obj.tag in self.PASS_TAGS:
                    pass
                else:
                    open('tag-not-found','a+').write("%s\n" % obj.tag)
                    raise IOError('TAG NOT FOUND')
            elif action == 'start-ns':
                print("%s: %s" % (action, obj))
            else:
                print("?", action)

    def get_triples(self):
        if not self.info:
            return
        triples = []
        docid = self.info['meta']['DOCID']
        for key, value in self.info['meta'].items():
            if isinstance(value, (list, tuple)):
                for val in value:
                    triples.append((docid, key, val))
            else:
                triples.append((docid, key, value))
        return list(set(triples))

    def rename(self):
        if hasattr(self, 'RENAME') and self.RENAME is not None:
            for old, new in self.RENAME.items():
                if old in self.info['meta']:
                    self.info['meta'][new] = self.info['meta'][old]
                    del self.info['meta'][old]

    def split_keywords(self, keys, sep=r','):
        return [x.upper()
                for x in
                list(filter(lambda x: len(x) > 0,
                            re.split(sep, keys)))]


class CMS_Consumer(Consumer):

    def identify(self):
        # LOGGER.debug('CMS_Consumer:identify')
        if 'KEYS' in self.info:
            self.info['meta']['CHIAVI'] = self.split_keywords(self.info['KEYS'], ':')
            self.info['meta']['NKEYS'] = self.info['meta']['CHIAVI']
        super(CMS_Consumer, self).identify()


class EXAG_Consumer(CMS_Consumer):
    Archivio = "EXAG"
    SUBST_ALL = (
        (r'.', r'<(/?)(BR|HR)>', u'<\\1P>', re.I),
        (r'.',
         r'<([^>]+)>', escape, re.I),
        (r'.*<\.\.\.>', r'(<\.\.\.>)', escape, re.I),
        (r'.*<[-\w\.]+@[\w\.]+>', r'(<[-\w\.]+@[-\w\.]+>)', escape, re.I),
        (r'^([^<]+(<p>)+)?(AGORA\': (MESSAGGI|CONFERENZA SEGRETERIA PR)|\d+, |From: |(([A-Z]+ )+- )?EMAIL (RICEVUTA|DAL WEB) |-----Messaggio originale-----)',
         r'<([^>]+)>', escape, re.I),
        (r'^([^<]+(<p>)+)?(Sommario|Da|Oggetto|Date sent|Message-Id|Subject|Ri|Re|Date|From|To):[ \t]',
         r'<([^>]+)>', escape, re.I),
        (r'^[^<]+(<p>)+(From) ',
         r'<([^>]+)>', escape, re.I),
    )
    SUBST = {
        'EXAG:16822': ((r'<bruceritchie @ gn.apc.org>', escape, re.I),),
        'EXAG:20098': ((r'<ene ', 'ene', re.I),),
        'EXAG:39898': ((r'<\.\.\.>', '...', re.I),),
        'EXAG:39964': (DETAG,),
        'EXAG:40106': (DETAG,),
        'EXAG:40156': (DETAG,),
        'EXAG:40161': (DETAG,),
        'EXAG:40166': (DETAG,),
        'EXAG:40299': (DETAG,),
        'EXAG:40324': (DETAG,),
        'EXAG:40449': (DETAG,),
        'EXAG:40985': (DETAG,),
        'EXAG:40987': (DETAG,),
        'EXAG:41485': (DETAG,),
        'EXAG:44035': (DETAG,),
        'EXAG:44061': (DETAG,),
        'EXAG:44228': (DETAG,),
        'EXAG:45015': (DETAG,),
        'EXAG:46281': (DETAG,),
        'EXAG:46282': (DETAG,),
        'EXAG:47098': (DETAG,),
        'EXAG:48793': (DETAG,),
        'EXAG:48895': (DETAG,),
    }

    def identify(self):
        # LOGGER.debug('EXAG_Consumer:identify')
        super(EXAG_Consumer, self).identify()
        self.info['meta']['AUTHOR'] = self.info['AUTHOR']
        self.info['meta']['TITLE'] = self.info['TITLE']
        self.info['meta']['DATE'] = self.info['DATE'].isoformat()



class MWDB1_Consumer(CMS_Consumer):
    Archivio = "MWDB1"

class RADIT_Consumer(CMS_Consumer):
    Archivio = "RADIT"

class MITT_Consumer(CMS_Consumer):
    Archivio = "MITT"


class ARCD_Consumer(Consumer):
    Archivio = "ARCD"

    RENAME = {
        'RIFERIMENTO': 'REFERENCE',
        'ARCHIVIO': 'ARCHIVE',
        'DATA': 'DATE',
        'SCHEDA':  'AID',
        'LINGUE': 'TRANSLATIONS',
        'CHIAVI': 'KEYS',
        'LINGUA': 'LANG',
        'AUTORE': 'AUTHOR',
        'TITOLO': 'TITLE',
    }

    def consume(self):
        event_types = ("start", "end")
        hparser = etree.HTMLPullParser(event_types)
        events = hparser.read_events()
        hparser.feed(self.document)
        self.consume_events(events)
        if self.info['p']:
            self.identify()
            self.rename()
            self.info['meta']['DOCID'] = "%s:%s" % (self.Archivio, self.info['DOCID'])
            self.info['meta']['REVISION'] = '0'
            self.info['empty'] = False
        else:
            self.info['empty'] = True

    def find_line(self, regexp, from_line=0):
        for num, line in enumerate(self.info['p'][from_line:]):
            if regexp.match(line.strip()):
                return num

    def find_abstract(self):
        abstr_regexp = re.compile(
            r'^(ABSTRACT|SOMMARIO|SUMARIO|SOMMAIRE|ZUSAMMENFASSUNG)', flags=re.I)
        self.abstr_begins = self.find_line(abstr_regexp)
        if self.abstr_begins is not None:
            paren_regexp = re.compile(r'^\(.*\)$')
            self.abstr_paren = self.find_line(paren_regexp, self.abstr_begins)
            if self.abstr_paren:
                self.abstr_end = self.abstr_begins + self.abstr_paren
            else:
                empty_regexp = re.compile(r'^$')
                self.abstr_empty = self.find_line(empty_regexp, self.abstr_begins)
                if self.abstr_empty:
                    self.abstr_end = self.abstr_begins + self.abstr_empty
                else:
                    self.abstr_end = self.abstr_begins
            self.abstract = '\n'.join([
                "<p>%s</p>" % line for line in self.info['p'][self.abstr_begins:self.abstr_end + 1]
            ])
            self.next_line = self.abstr_end + 1

    def identify(self):
        self.next_line = 0
        self.find_abstract()
        if self.abstract:
            self.info['abstract'] = self.abstract
        #
        self.info['p'] = self.info['p'][self.next_line:]
        #
        if 'CHIAVI' in self.info['meta']:
            if len(self.info['meta']['CHIAVI']) > 0:
                self.info['meta']['CHIAVI'] = [x.upper() for x in
                                               list(filter(lambda x: len(x) > 0, self.info['meta']['CHIAVI'].split(',')))]
                self.info['meta']['NKEYS'] = self.info['meta']['CHIAVI']
                # self.info['meta']['CHIAVI'] = ' '.join(["[[Category: %s]]" % x
                # for x in self.info['meta']['CHIAVI'].split(',')])

        if 'ARCHIVIO' in self.info['meta']:
            self.info['ARCHIVIO'] = self.info['meta']['ARCHIVIO']
        if 'LINGUA' in self.info['meta']:
            self.info['LINGUA'] = self.info['meta']['LINGUA']
        if 'SOURCE' in self.info:
            self.info['meta']['SOURCE'] = self.info['SOURCE']

    def consume_text(self, obj):
        self.info[obj.tag] = obj.text

    def consume_start_title(self, obj):
        self.info['meta']['TITOLO'] = obj.text
        self.consume_text(obj)

    def consume_start_a(self, obj):
        """Questi tag a indicano le traduzioni"""
        langs = {
            'italy': 'IT',
            'spain': 'ES',
            'uk': 'EN',
            'france': 'FR',
            'germany': 'DE',
            'esperanto': 'EP'
        }
        href = "[[ARCD." + obj.attrib['href'][14:-4] + "]]"
        lang = obj.getchildren()[0].attrib['src'][10:-4]
        if lang in langs:
            lang = 'trans_' + langs[lang]
        else:
            lang = 'trans_UNKNOWN'
            href += ' (' + lang + ')'
        self.info['meta'][lang] = href

    def consume_start_h3(self, obj):
        name = obj.getchildren()[0].text
        self.info[obj.tag] = name

    def consume_start_h4(self, obj):
        self.consume_text(obj)

    def consume_start_h2(self, obj):
        self.consume_text(obj)

    def consume_start_h1(self, obj):
        self.consume_text(obj)

    def consume_start_meta(self, obj):
        name = obj.attrib['name']
        value = obj.attrib['content']
        self.consume_keyvalue(obj, name, value)

    def consume_keyvalue(self, obj, key, value):
        if obj.tag not in self.info:
            self.info[obj.tag] = {}
        self.info[obj.tag][key] = value

    def consume_start_p(self, obj):
        text = obj.text
        if text:
            if re.match(r'^(ARC|NOT).*(IT|EN|FR|ES|EP|DE)$', text):
                self.info['id'] = text
            elif re.match(r'^$', text):
                pass
            else:
                self.info['p'].append(text)


class KwConsumer(object):
    """Il Consumer legge l'articolo e trova le variabili informative che registra in info

    Le chiavi di info sono:

    p : contiene le linee del testo
    SOURCE : contiene l'identificativo della fonte documentale
    meta : contiene un dict con le informazioni che saranno riportate nella pagina
    abstract : sommario del testo

    in meta le chiavi importanti sono:

    PID : identificazione del documento (TODO: rinominare in DOCID)
    LINGUA : Lingua del documento
    SOURCE : identificativo della fonte documentale
    CHIAVI : keywords/categorie (list)
    RIFERIMENTO : 
    trad_<XX> : dove XX è il codice della lingua - identificativo della traduzione

    """
    SUBST_ALL = (
        (r'.', r'<(/?)(BR|HR)>', u'<\\1P>', re.I),
    )
    SUBST = {}
    DEBUG_FILE = False
    PASS_TAGS = ('html', 'head', 'body', 'blockquote', 'i', 'img', 'snip', 'font', 
                 'strong', 'br', 'h1', 'h2', 'h3','a', 'ol', 'ul', 'object', 'pre',
                 'em', 'b', 'center', 'div', 'h4', 'hr', 'li', 'll', 'param', 
                 'script', 'span', 'table', 'td ', 'th ', 'tr', 'u')
    SUBST = LOCAL_SUBST

    def __init__(self, arc, pid, html_document, *args, **kwargs):
        self.pid = int(pid)
        self.Archivio = arc
        self.document = html_document
        self.info = {
            'p': [],
            'SOURCE': arc,
            'DOCID': self.pid,
            'meta': {'PID':  self.pid},
        }
        self.info.update(**kwargs)
        self.abstract = None
        self.docid = "%s:%s" % (self.Archivio,self.pid) 
        self.rewrite()

    def rewrite(self):
        if self.SUBST_ALL:
            for target, regexp, subst, flags in self.SUBST_ALL:
                if re.match(target, self.document, re.I):
                    self.document = re.sub(regexp, subst, self.document, flags=flags)
        if self.SUBST:
            if self.docid in self.SUBST:
                for regexp, subst, flags in self.SUBST[self.docid]:
                    self.document = re.sub(regexp, subst, self.document, flags=flags)

    def consume(self):
        if self.DEBUG_FILE:
            self.save_page()
        #
        event_types = ("start",)
        hparser = etree.HTMLPullParser(event_types)
        events = hparser.read_events()
        hparser.feed(self.document)
        self.consume_events(events)
        #
        self.info['empty'] = False
        if self.info['p']:
            self.identify()
        else:
            self.info['empty'] = True
        if self.DEBUG_FILE:
            self.save_info()

    def identify(self):
        # LOGGER.debug('CON:identify')
        self.info['meta']['DOCID'] = "%s:%d" % (self.Archivio, self.info['meta']['PID'])
        self.info['meta']['VER'] = '0'

    def save_page(self):
        fname = 'doc.html'
        LOGGER.info('Saving page in %s' % fname)
        with open(fname, 'wb') as f:
            f.write(self.document.encode('utf-8'))

    def save_info(self):
        fname = 'doc'
        LOGGER.info('Saving page in %s' % fname)
        self.info['DATE'] = self.info['DATE'].isoformat()
        with open(fname + ".json", 'w') as f:
            json.dump(self.info, f, indent=True, skipkeys=True)
            f.write('-' * 50)
            f.write(pformat(self.info))

    def consume_start_p(self, obj):
        text = obj.text
        if text:
            if re.match(r'^$', text):
                pass
            else:
                self.info['p'].append(text)

    def consume_events(self, events):
        for action, obj in events:
            if action in ('start', 'end'):
                # print (action, obj.tag)
                method = "consume_%s_%s" % (action, obj.tag)
                if hasattr(self, method):
                    method = getattr(self, method)
                    method(obj)
                elif action in ('end'):
                    pass
                elif obj.tag in self.PASS_TAGS:
                    pass
                else:
                    open('tag-not-found','a+').write("%s\n" % obj.tag)
                    open('last-text','w+').write(self.document)
                    continue
                    # raise IOError('TAG NOT FOUND %s' % obj.tag)
            elif action == 'start-ns':
                print("%s: %s" % (action, obj))
            else:
                print("?", action)

    def get_triples(self):
        if not self.info:
            return
        triples = []
        docid = self.info['meta']['DOCID']
        for key, value in self.info['meta'].items():
            if isinstance(value, (list, tuple)):
                for val in value:
                    triples.append((docid, key, val))
            else:
                triples.append((docid, key, value))
        return list(set(triples))

    def rename(self):
        if hasattr(self, 'RENAME') and self.RENAME is not None:
            for old, new in self.RENAME.items():
                if old in self.info['meta']:
                    self.info['meta'][new] = self.info['meta'][old]
                    del self.info['meta'][old]

    def split_keywords(self, keys, sep=r','):
        return [x.upper()
                for x in
                list(filter(lambda x: len(x) > 0,
                            re.split(sep, keys)))]
