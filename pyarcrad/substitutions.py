import html
import re


def hescape(x):
    return html.escape(x.group(0))


LOCAL_SUBST = {
    'MITT:35536': ((r'\<si ', hescape, re.I),),
    'EXAG:45015': ((r'i\s*\<litical', 'litical', re.I + re.M),),
    'EXAG:47098': ((re.escape('<better against="" all="" an="" and="" anything="" arm="" attempt="" daughter-in-law="" first="" if="" in="" it="" make="" mother-in-law="" murderous="" of="" order="" ourselves="" pci="" present="" speak="" talk="" the="" them="" to="" understand.="" was="">'), 'better against all an and anything arm attempt daughter-in-law first if in it make mother-in-law murderous of order ourselves pci present speak talk the them to understand. was', re.I),
                   (re.escape('</better>'),'',re.I)), 
    }
