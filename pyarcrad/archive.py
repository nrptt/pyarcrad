import itertools
import logging
from collections import UserDict, namedtuple
from io import StringIO
from string import Template

from .consumer import ARCD_Consumer, EXAG_Consumer, MITT_Consumer, MWDB1_Consumer, RADIT_Consumer
from .db import ARCD_DB, EXAG_DB, MITT_DB, MWDB1_DB, RADIT_DB
from .exporter import ARCD_Exporter, EXAG_Exporter, MITT_Exporter, MWDB1_Exporter, RADIT_Exporter
from .utils import PostInfo, triples_to_dict

logger = logging.getLogger(__name__)

class Document(object):

    Container = None
    Consumer = None
    Exporter = None

    def __init__(self):
        self.container = None
        self.consumer = None
        self.exporter = None
        self.pid = None
        self._triples = None
        self.info = None
        self.graph = None

    @property
    def SOURCE(self):
        return self.Container.SOURCE

    @property
    def ids(self):
        return self.Container().ids()

    @property
    def dtriples(self):
        return triples_to_dict(self.triples)

    @property
    def triples(self):
        if self._triples:
            return self._triples
        

    def get(self, pid):
        "Usa il Container per ottenere i dati base"
        if self.Container and self.Exporter and self.Consumer:
            self.container = self.Container()
            keys = self.container.get(pid)
            if 'TEXT' in keys and keys['TEXT']:
                self.document = keys['TEXT']
                keys.setupMeta()
                pid = keys['meta']['PID']
                self._triples = self.container.get_triples(pid)
                self.consumer = self.Consumer(pid, self.document, **keys)
                self.consumer.consume()
                if not self.consumer.info['empty']:
                    self._triples.extend(self.consumer.get_triples())
                    self._triples = list(set(self.triples))
                    self.exporter = self.Exporter(self.triples, **self.consumer.info)
                    self.graph = self.exporter.graph
                    self.info = self.consumer.info
                    self.docid = self.consumer.info['meta']['DOCID']
                    self.source = self.consumer.info['meta']['SOURCE']
                    self.document = self.exporter.output()

    def range(self, start, stop):
        self.container = self.Container()
        return self.container.range(start, stop)



class ARDB(Document):

    Container = ARCD_DB
    Consumer = ARCD_Consumer
    Exporter = ARCD_Exporter


class EXAG(Document):

    Container = EXAG_DB
    Consumer = EXAG_Consumer
    Exporter = EXAG_Exporter


class MWDB1(Document):

    Container = MWDB1_DB
    Consumer = MWDB1_Consumer
    Exporter = MWDB1_Exporter


class MITT(Document):

    Container = MITT_DB
    Consumer = MITT_Consumer
    Exporter = MITT_Exporter



class RADIT(Document):

    Container = RADIT_DB
    Consumer = RADIT_Consumer
    Exporter = RADIT_Exporter




Recognizers = {
    'ARDB': ARDB,
    'EXAG': EXAG,
    'MWDB1': MWDB1,
    'MITT': MITT,
    'RADIT': RADIT,
}

Post = namedtuple('Post', ['pid', 'source', 'document', 'info', 'triples', 'graph'])


class Corpus(UserDict):
    POST = """
    <page>
      <title>$ID</title>
      <ns>0</ns>
      <id>$PID</id>
      <revision>
        <id>1</id>
        <timestamp>2016-09-29T09:23:52Z</timestamp>
        <contributor>
          <username>Admin</username>
          <id>1</id>
        </contributor>
        <comment>$LOG</comment>
        <model>wikitext</model>
        <format>text/x-wiki</format>
        <text xml:space="preserve" bytes="$BYTES">$TEXT</text>
      </revision>
    </page>
    """

    XMLHEADER = """
    <mediawiki xmlns="http://www.mediawiki.org/xml/export-0.10/" 
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.mediawiki.org/xml/export-0.10/ http://www.mediawiki.org/xml/export-0.10.xsd" 
               version="0.10" 
               xml:lang="it">
      <siteinfo>
        <sitename>AR</sitename>
        <dbname>ar_wiki</dbname>
        <base>http://localhost:8888/ar/index.php/Pagina_principale</base>
        <generator>MediaWiki 1.27.1</generator>
        <case>first-letter</case>
        <namespaces>
          <namespace key="-2" case="first-letter">Media</namespace>
          <namespace key="-1" case="first-letter">Speciale</namespace>
          <namespace key="0" case="first-letter" />
          <namespace key="1" case="first-letter">Discussione</namespace>
          <namespace key="2" case="first-letter">Utente</namespace>
          <namespace key="3" case="first-letter">Discussioni utente</namespace>
          <namespace key="4" case="first-letter">AR</namespace>
          <namespace key="5" case="first-letter">Discussioni AR</namespace>
          <namespace key="6" case="first-letter">File</namespace>
          <namespace key="7" case="first-letter">Discussioni file</namespace>
          <namespace key="8" case="first-letter">MediaWiki</namespace>
          <namespace key="9" case="first-letter">Discussioni MediaWiki</namespace>
          <namespace key="10" case="first-letter">Template</namespace>
          <namespace key="11" case="first-letter">Discussioni template</namespace>
          <namespace key="12" case="first-letter">Aiuto</namespace>
          <namespace key="13" case="first-letter">Discussioni aiuto</namespace>
          <namespace key="14" case="first-letter">Categoria</namespace>
          <namespace key="15" case="first-letter">Discussioni categoria</namespace>
          <namespace key="828" case="first-letter">Modulo</namespace>
          <namespace key="829" case="first-letter">Discussioni modulo</namespace>
        </namespaces>
      </siteinfo>
    """

    XMLFOOTER = '</mediawiki>'

    def __init__(self, name, start=None, stop=None):
        self.name = name
        self.archivio = Recognizers[name]()
        if start is not None:
            start = int(start)
        if stop is not None:
            stop = int(stop)
        self.range = self.archivio.range(start, stop)
        self.start, self.stop = self.range[0], self.range[-1]
        self.SOURCE = "%s(%d,%d)" % (self.archivio.Container.SOURCE, self.start, self.stop)
        self.data = {}
        self.unread = []

    @property
    def triples(self):
        return list(itertools.chain(
            *[ v.triples for v in self.data.values() ]
            ))

    def load(self,store):
        L = len(self.range)
        for j, pid in enumerate(self.range):
            logger.debug('Leggo il post n. \'%d\' (%d/%d)' % (pid, j, L))
            try:
                self.archivio.get(pid)
            except Exception as err:
                logger.error("%s:%s:%s" %(__file__,0,str(err).encode('utf-8')))
                logger.error(u"Non posso leggere il post n. %d dall'archivio %s" %
                             (pid, self.archivio.Container.SOURCE))
                self.unread.append(pid)
            if self.archivio.info:
                post = Post(pid,
                            self.archivio.source, self.archivio.document, 
                            self.archivio.info, self.archivio.triples, 
                            self.archivio.graph)
                self.data[pid] = post
                if store:
                    store.add(self.archivio.SOURCE, self.archivio.triples)
            else:
                logger.error(u"Il post n. %d non è disponibile nell'archivio %s" %
                             (pid, self.archivio.Container.SOURCE))
                self.unread.append(pid)


    def dumps(self):
        info = {
            'LOG': 'Initial Import',
            'PID': 1,
            'ID': None,
            'TEXT': None,
        }
        tpost = Template(self.POST)
        with StringIO() as fh:
            fh.write(self.XMLHEADER)
            L = len(self.range)
            for j, (pid, post) in enumerate(self.data.items()):
                logger.debug('Leggo il post n, \'%d\' (%d/%d)' % (pid, j, L))
                logger.debug("Carico dalla fonte: %s" % post.source)
                info['PID'] = pid
                info['ID'] = post.source
                info['TEXT'] = post.document
                info['BYTES'] = len(info['TEXT'])
                fh.write(tpost.substitute(info))
            fh.write(self.XMLFOOTER)
            return fh.getvalue()

CorpusDef = namedtuple('CorpusDef', ['corpus', 'start', 'stop'])


class Archive(UserDict):

    def __init__(self, corpusdefs, store=None):
        self.corpusdefs = corpusdefs
        self.data = {}
        self.store = store

    @property
    def triples(self):
        return list(itertools.chain(
            *[ v.triples for v in self.data.values() ]
            ))

    def __enter__(self):
        for archive, start, stop  in self.corpusdefs:
            corpus = Corpus(archive, start, stop)
            corpus.load(self.store)
            self.data[corpus.SOURCE] = corpus
        return self

    def __exit__(self,exception_type, exception_value, traceback):
        pass
