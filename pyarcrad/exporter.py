# -*- coding: utf-8 -*-

import atexit
import codecs
import glob
import html
import logging
import os
import re
import shelve
import sys
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from datetime import datetime
from io import StringIO
from os.path import exists, expanduser, expandvars, getsize, join
from pathlib import Path
from pprint import pprint
from string import Template
from textwrap import dedent

# from boltons.cacheutils import LRI, LRU, cached, cachedmethod
from bs4 import BeautifulSoup

from .consumer import KwConsumer
from .utils import PostInfo, Progress, triples_to_dict, triples_to_graph

LOGGER = logging.getLogger(__name__)

def escape(x):
    PASS_TAGS = ('html', 'head', 'body', 'blockquote', 'i', 'img', 'snip', 'font', 
                 'strong', 'br', 'h1', 'h2', 'h3','a', 'ol', 'ul', 'object', 'pre',
                 'em', 'b', 'center', 'div', 'h4', 'hr', 'li', 'll', 'param', 
                 'script', 'span', 'table', 'td ', 'th ', 'tr', 'u')
    tag = x.group(1)
    if tag.lower() in PASS_TAGS:
        return "<" + tag + ">"
    return html.escape("<" + tag + ">")

class Exporter(object):
    """ Prende le informazioni del DB e del consumer e compone l'output """

    def __init__(self, triples, *args, **kwargs):
        self.info = PostInfo(**kwargs)
        self.info.setupMeta()
        self.triples = triples

    def compose_from_dict(self, info):
        """compose a dict in a key=value string"""
        newvals = info.items()
        if hasattr(self, 'TRANSFORM'):
            newvals = [(k, v if k not in self.TRANSFORM else self.TRANSFORM[k](v))
                       for k, v in newvals]
        return '\n'.join([
            "| %s=%s" % kv for kv in sorted(newvals)
        ])

    def compose(self, boxname, info):
        """compose the infobox"""
        infos = self.compose_from_dict(info)
        infos += "\n| LDATE = %s " % datetime.now().isoformat()
        box = """{{%s\n%s\n}}""" % (boxname, infos)
        return box

    def output(self):
        """return the wiki post"""
        infobox = self.compose(self.TEMPLATE, self.info['meta'])
        title = self.info.get('TITLE', "***TITOLO VUOTO***")
        abstract = ""
        if 'abstract' in self.info:
            abstract = "{{abstract|%s}}<br/>" % self.info['abstract']
        categories = ""
        for lab, cat in (('Fonte', 'SOURCE'),
                         ('Archivio', 'ARCHIVIO'),
                         ('Lingua', 'LINGUA')):
            if cat in self.info:
                categories += "[[Category:%s:%s]]" % (lab, self.info.xget(cat))
        content = ''
        if 'p' in self.info:
            content = '\n'.join(["<p>%s</p>" % p for p in self.info['p']])
        text = """%s\n%s\n%s\n\n%s\n""" % (
            infobox, abstract, content, categories)
        return text

    def infobox(self):
        if self.INFOBOX:
            docid = self.info['meta']['DOCID']
            output = []
            aDict = triples_to_dict(self.triples)
            for section in self.INFOBOX:
                if section in aDict[docid]:
                    value = aDict[docid][section]
                    output.append((section, value))
            return output

    @property
    def graph(self):
        return triples_to_graph('exp', self.triples)


class ARCD_Exporter(Exporter):
    TEMPLATE = "ARCD"
    INFOBOX = (
        'DOCID',
        'DATE'
        'SOURCE',
        'ARCLABEL',
        'ARCID',
        'ARCNAME',
        'ARCNUM',
        'AUTHOR',
        'TITLE',
        'LANG',
        'KEYS',
        'trad_IT'
        'trad_EN'
        'trad_FR',
        'trad_ES',
        'trad_DE',
        'trad_EP'
    )
    TRANSFORM = {
        'KEYS': lambda x: ' '.join(["[[Category:%s]]" % s for s in x]),
        'NKEYS': lambda x: ', '.join(["%s" % s for s in x]),
    }


class EXAG_Exporter(Exporter):
    TEMPLATE = "EXAG"


class MWDB1_Exporter(Exporter):
    TEMPLATE = "MWDB"


class MITT_Exporter(Exporter):
    TEMPLATE = "MITT"


class RADIT_Exporter(Exporter):
    TEMPLATE = "RADIT"

class KwExporter(object):
    """ Prende le informazioni del DB e del consumer e compone l'output """
    TITLE = 'TITLE'
    TEMPLATE = 'Arcradbox'
    TRANSFORM = {
        'NKEYS': lambda x: ' '.join(["[[Category:%s]]" % s for s in sorted(x)]),
        'KEY': lambda x: ', '.join(["[[:Category:%s|%s]]" % (s,s) for s in sorted(x)]),
    }
    EXCLUDE =  ('ABSTRACT', 'TEXT', 'ATTRIBUTES')
    

    # see https://www.mediawiki.org/wiki/Help:Export
    # models are in https://github.com/wikimedia/mediawiki/blob/master/docs/contenthandler.txt
    POST_MODEL = u"""\
    <page>
      <title>$POST_TITLE</title>
      <ns>$POST_NAMESPACE</ns>
      <id>$POST_PID</id>
      <revision>
        <id>$POST_REVID</id>
        <timestamp>$POST_REVDATE</timestamp>
        <contributor>
          <username>$POST_USER</username>
          <id>$POST_USERID</id>
        </contributor>
        <comment>$POST_LOG</comment>
        <model>$POST_MODEL</model>
        <format>$POST_FORMAT</format>
        <text xml:space="preserve" bytes="$POST_BYTES">$POST_TEXT</text>
      </revision>
    </page>
    """

    def __init__(self, *args, **kwargs):
        if 'LANG' not in kwargs or len(kwargs.get('LANG')[0])==0:
            kwargs['LANG'] = [ 'IT', ]
        if 'KEY' not in kwargs:
            kwargs['KEY'] = []
        if 'NKEYS' not in kwargs and 'KEY' in kwargs:
            kwargs['NKEYS'] = [ x for x in kwargs['KEY'] ]
        self.info = PostInfo(**kwargs)
        if 'DOCID' not in self.info or isinstance(self.info['DOCID'], int):
            if 'PID' in self.info and 'SOURCE' in self.info:
                self.info['DOCID'] = self.info['PID'][0] + ":" + self.info['SOURCE'][0]
            else:
                # from pprint import pprint
                # pprint(kwargs)
                # print('THIS DOCID NOT EXPORTED')
                # import pdb; pdb.set_trace()
                raise ValueError('DOCID not in info')
        if 'ARCID' not in self.info:
            self.info['ARCPID'] = self.info['PID']
            self.info['ARCLABEL'] = self.info['SOURCE']
            self.info['ARCID'] = '0'

    def infobox_from_dict(self, info):
        """compose a dict in a key=value string"""
        keys = info.keys()
        newvals = info
        if hasattr(self, 'TRANSFORM'):
            newvals = {}
            for k in keys:
                if k in info and k not in self.EXCLUDE:
                    val = info.xget(k) 
                    t = 0
                    if k in self.TRANSFORM:
                        arg = info[k]
                        if not isinstance(arg,list):
                            arg = arg.split(',')
                        val = self.TRANSFORM[k](arg)
                        t = 1
                    newvals[k] = val 
        return '\n'.join([
            "| %s=%s" % (k,html.escape(newvals.get(k))) for k in sorted(keys) if k not in self.EXCLUDE
        ])

    def infobox(self, boxname, info):
        """compose the infobox"""
        infos = self.infobox_from_dict(info)
        infos += "\n| X_LDATE = %s " % datetime.now().isoformat()
        infos += "\n| X_STATUS = L " 
        box = """{{%s\n%s\n}}""" % (boxname, infos)
        return box

    @property
    def text(self):
        """return the wiki post"""
        infobox = self.infobox(self.TEMPLATE, self.info)
        title = html.escape("<h1>"+self.info.xget('TITLE', "***TITOLO VUOTO***")+"</h1>")
        abstract = ""
        if 'ABSTRACT' in self.info:
            abstract = "%s<br/>" % html.escape(self.info['ABSTRACT'][0])
        categories = ""
        for lab, cat in (('Fonte', 'SOURCE'),
                         ('Archivio', 'ARCHIVIO'),
                         ('Archivio', 'ARCLABEL'),
                         ('Lingua', 'LINGUA'),
                         ('Lingua', 'LANG'),
        ):
            if cat in self.info:
                val = self.info.xget(cat)
                if len(val)>0:
                    categories += "[[Category:%s:%s]]" % (lab, val)
        content = ''
        content = self.info.xget('TEXT')
        key = self.info.get('DOCID')[0]
        (arc,pid) = key.split(':')
        soup = BeautifulSoup(content,"lxml")
        content = soup.prettify()
        consumer = KwConsumer(arc,pid,content,**self.info)
        try:
            consumer.consume()
        except OSError as exc:
            print(arc,pid,content)
            raise
        content = '\n'.join(
            ['<p>%s</p>' % p for p in consumer.info['p'] if len(p)>0 ]
        )
        content = html.escape(content)
        text = """%s\n%s\n%s\n\n%s\n""" % (
            infobox, abstract,  content, categories)
        return text

    @property
    def namespace(self):
        ## TODO: make this better
        source = self.info.xget('SOURCE')
        if source == 'EXAG':
            return 20
        if source == 'MITT':
            return 22
        return 4

    @property
    def title(self):
        docid = re.sub(':','-',self.info.xget('DOCID'))
        old = self.info.xget('TITLE','*** VUOTO ***')
        title =  old.strip() + " (" + docid + ")"

        # 1. Base names beginning with a lower-case letter (in any
        # alphabet), depending on the setting of $wgCapitalLinks. Note
        # that a title can be displayed with an initial lower-case
        # letter, using DISPLAYTITLE or the {{lowercase title}}
        # template. This does not fix every occurrence, like the
        # history, edit, or log pages (phab:T55566) - or the browser
        # address bar (phab:T63851), but only affects the page title
        # on the rendered HTML page and tab/window title bars.

        title = re.sub(r'^.',lambda x: x.group(0).upper(), title)

        # 2. Titles starting with a lowercase letter are automatically
        # converted to leading uppercase


        # 3. Titles containing the characters # < > [ ] | { } _ (which
        # have special meanings in Wiki syntax), the non-printable
        # ASCII characters 0–31, the "delete" character 127, or HTML
        # character codes such as &amp;. Note that the plus sign + is
        # allowed in page titles, although in the default setup for
        # MediaWiki it is not. This is configured by setting the value
        # of $wgLegalTitleChars in LocalSettings.php.

        title = re.sub(r'\|',':',title)

        title = re.sub(r'[#<>\[\]\|\{\}_]','?',title)

        # 4. Special characters like ( ) & + are translated into their
        # equivalent %-hex notation

        

        # 5. Base names beginning with a colon (:).
        title = re.sub(r'^:','?',title)

        # 6. Base names equal to "." or "..", or beginning "./" or
        # "../", or containing "/./" or "/../", or ending "/." or
        # "/..".

        title = re.sub(r'^\.$','dot-',title)
        title = re.sub(r'^\.\.$','dot-dot-',title)

        title = re.sub(r'^\.\.\/','dot-dot-slash-',title)
        title = re.sub(r'^\.\/','dot-slash-',title)

        title = re.sub(r'\/\.\.$','-slash-dot-dot',title)
        title = re.sub(r'\/\.','-slash-dot',title)

        title = re.sub(r'\/\.\.\/','-slash-dot-dot-slash-',title)
        title = re.sub(r'\/\.\/','-slash-dot-slash-',title)



        # 7. Base names whose length exceeds 255 bytes. Be aware that
        # non-ASCII characters may take up to four bytes in UTF-8
        # encoding, so the total number of characters you can fit into
        # a title may be less than 255.

        if len(title)>255:
            title = title[:255]

        # 8. Titles beginning with a namespace alias (WP:, WT:,
        # Project:, Image:, on Wikipedia). For example, the name
        # Project:A-Kon is not possible if Project: is set as a
        # namespace alias.

        # while ':' in title:
        #    title = re.sub(r':',' : ',title)


        # 9. Titles beginning with a prefix that refers to another
        # project, including other language Wikipedias, e.g. "fr:"
        # (see Interwiki linking and Interlanguage links). For
        # example, an article about the album "Q: Are We Not Men? A:
        # We Are Devo!" cannot have that exact name, as the "q:"
        # prefix leads to Wikiquote. (The restriction includes the
        # prefixes "w:" and "en:" that refer to English Wikipedia
        # itself. This self-reference restriction does not apply on
        # all projects; for example, Wikiquote supports titles
        # beginning "Q:".)

        # 10. Titles beginning with any non-standard capitalization of
        # a namespace prefix, alias or interwiki/interlanguage prefix,
        # or any of these with a space (underscore) before or after
        # the colon. For example, it is not possible for a title to
        # begin "HELP:", "HeLp:", "Help :" or "Help:_".

        # 11. Titles consisting of only a namespace prefix, with
        # nothing after the colon.

        # 12. Titles beginning or ending with a space (underscore), or
        # containing two or more consecutive spaces (underscores).
        
        title = re.sub(r'_+$','',title)

        # 13. Title cannot contain 3 or more continuous tildes. (~~~)

        title = re.sub(r'~~~+','~ ~ ~',title)


        # 14. A title can normally contain the character %. However it
        # cannot contain % followed by two hexadecimal digits (which
        # would cause it to be converted to a single character, by
        # percent-encoding).
        return title


    
    @property
    def post(self):
        p = {
            'POST_TITLE': None,
            'POST_NAMESPACE': 0,
            'POST_PID': None,
            'POST_REVID': 1,
            'POST_REVDATE': None,
            'POST_USER': 'admin',
            'POST_USERID': 1,
            'POST_LOG': None,
            'POST_MODEL': 'wikitext',
            'POST_FORMAT': 'text/x-wiki',
            'POST_BYTES': 0,
            'POST_TEXT': None,
        }
        ARC,PID = self.info.xget('DOCID').split(':')
        p['POST_NAMESPACE'] = 0 # self.namespace 
        p['POST_TITLE'] = self.title
        # p['POST_TITLE'] = re.sub(r':','&#58;',html.escape(self.info.xget('TITLE','*** VUOTO ***')))
        # p['POST_PID'] = self.info.xget('KID')
        p['POST_PID'] = PID
        p['POST_REVDATE'] = self.info.xget('DATE','2016-01-01T00:00:000')
        p['POST_TEXT'] = self.text
        p['POST_BYTES'] = len(p['POST_TEXT'])
        p['POST_LOG'] = self.info.xget('LOG',('Initial Import',))
        post = str(dedent(Template(self.POST_MODEL).substitute(p)))
        return post
        
    def dump(self, path, force=False):
        filename = path / (self.info.xget('DOCID') + ".xml")
        if force or not filename.exists():
            with filename.open('wb') as fh:
                post = self.post
                print("Exporting %d bytes into %s" % (len(post),str(filename)))
                fh.write(post)
        else:
            print("Not saving "+filename)

    def dumps(self): 
        return self.info.xget('DOCID'),self.post
        # return self.info.xget('DOCID'),self.post.encode('utf-8','ignore')



# Persistent LRU cache for the parts of speech




class KwCorpusExporter(object):

    XMLHEADER = u"""
    <mediawiki xmlns="http://www.mediawiki.org/xml/export-0.10/" 
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:schemaLocation="http://www.mediawiki.org/xml/export-0.10/ http://www.mediawiki.org/xml/export-0.10.xsd" 
               version="0.10" 
               xml:lang="it">
      <siteinfo>
        <sitename>AR</sitename>
        <dbname>ar_wiki</dbname>
        <base>http://localhost:8888/ar/index.php/Pagina_principale</base>
        <generator>MediaWiki 1.27.1</generator>
        <case>first-letter</case>
        <namespaces>
          <namespace key="-2" case="first-letter">Media</namespace>
          <namespace key="-1" case="first-letter">Speciale</namespace>
          <namespace key="0" case="first-letter" />
          <namespace key="1" case="first-letter">Discussione</namespace>
          <namespace key="2" case="first-letter">Utente</namespace>
          <namespace key="3" case="first-letter">Discussioni utente</namespace>
          <namespace key="4" case="first-letter">AR</namespace>
          <namespace key="5" case="first-letter">Discussioni AR</namespace>
          <namespace key="6" case="first-letter">File</namespace>
          <namespace key="7" case="first-letter">Discussioni file</namespace>
          <namespace key="8" case="first-letter">MediaWiki</namespace>
          <namespace key="9" case="first-letter">Discussioni MediaWiki</namespace>
          <namespace key="10" case="first-letter">Template</namespace>
          <namespace key="11" case="first-letter">Discussioni template</namespace>
          <namespace key="12" case="first-letter">Aiuto</namespace>
          <namespace key="13" case="first-letter">Discussioni aiuto</namespace>
          <namespace key="14" case="first-letter">Categoria</namespace>
          <namespace key="15" case="first-letter">Discussioni categoria</namespace>
          %s
          <namespace key="828" case="first-letter">Modulo</namespace>
          <namespace key="829" case="first-letter">Discussioni modulo</namespace>
        </namespaces>
      </siteinfo>
    """

    XMLFOOTER = '</mediawiki>'

    NAMESPACES = [ 'EXAG', 'MITT' ]
    NSPACE_ID  = 20

    # cached_data = shelve.open('cached_data', writeback=True)

    def __init__(self, corpus=None):
        self.corpus = corpus
        self.net_exported = []
        # atexit.register(KwCorpusExporter.cached_data.close)
        # Retrieve or create the "dumps" cache
        # self.cache = self.cached_data.setdefault('dumps', LRU(max_size=100000))
        # self.cache = LRU(max_size=1000000)

    def old_dump(self, path,force=False):
        path = path.expanduser()
        path.mkdir(parents=True,exist_ok=True)
        print("Expoting in "+str(path))
        not_exported = []
        exported = []
        for kid,post in self.corpus.items():
            try:
                post['KID']=kid,
                exporter = KwExporter(**post)
                exporter.dump(path, force)
                exported.append(kid)
            except ValueError as exc:
                # print('POST %s not exported' % kid)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                not_exported.append((kid,(exc_type,exc_value,exc_traceback)))
                continue
        return exported, not_exported

    def dump(self, path, force=None):
        exported, not_exported = self.dumps(force)
        path = path.expanduser()
        path.mkdir(parents=True,exist_ok=True)
        LOGGER.info("Expoting in "+str(path))
        for (kid,docid,post) in exported:
            filename = path / (docid + ".xml")
            if force or not filename.exists():
                with filename.open('wb') as fh:
                    LOGGER.info("Exporting %d bytes into %s" % (len(post),str(filename)))
                    fh.write(post)
            else:
                LOGGER.warning("Not saving "+filename)

    def dumps(self):
        def dump_chunk(progress,els,base,chunk_length):
            LOGGER.info("Calc Chunk beginning %d" % (base))
            exported = list()
            not_exported = list()
            for n in range(base, base+chunk_length):
                try:
                    (kid,post) = els[n]
                except IndexError:
                    continue
                try:
                    progress.notify(kid,verbose=True)
                    post['KID']=kid,
                    exporter = KwExporter(**post)
                    docid, dump = exporter.dumps()
                    exported.append((kid,docid,dump))                    
                except (RecursionError,ValueError) as exc:
                    # LOGGER.error('POST %s not exported' % kid)
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    not_exported.append((kid,(exc_type,exc_value,exc_traceback)))
            return exported, not_exported
        def dump_collection(workers, els):
            from math import ceil
            chunk_length = int(ceil(len(els) / workers))            
            exported = list()
            not_exported = list()           
            with Progress(self.corpus.keys(),100,verbose=True) as progress:
                for n in range(workers):
                    exp, not_exp = dump_chunk(progress, els, n * chunk_length, chunk_length)
                    LOGGER.info('chunk %d recovered (base=%d)' % (n,n * chunk_length))
                    exported.extend(exp)
                    not_exported.extend(not_exp)
            return exported, not_exported                        
        self.not_exported = []
        els = list(self.corpus.items())
        exported, not_exported = dump_collection(8,els)
        return exported, not_exported

    @property
    def namespaces(self):
        nid = self.NSPACE_ID
        nstr = ""
        for name in self.NAMESPACES:
            namespace = """          <namespace key="%s" case="first-letter">%s</namespace>\n"""
            nstr += namespace % (nid,name)
            nid += 1            
            namespace = """          <namespace key="%s" case="first-letter">%s</namespace>\n"""
            nstr += namespace % (nid,"Discussioni di "+name)
            nid += 1
        return nstr 

    def dump_path_into_xml_file(self, outfile, path, subj=None, _glob=None, force=False, limit=2500):
        globs = ( _glob if _glob else "*" ) + ".xml"
        files = path.glob(globs)
        xmlhead = self.XMLHEADER % self.namespaces
        numfiles = len(files)
        numpacks = int(len(files) / limit) + 1        
        self.delete_files(outfile)
        genfile = re.sub(r'\.xml','%03d.xml',outfile)
        genfiled = re.sub(r'\.xml','<n>.xml',outfile)
        print("Save %d files in %d packs into %s" % (numfiles, numpacks, genfiled))
        n = -1
        blocks = []
        # Compose files in blocks
        for n in range(numpacks):
            base = n * limit
            with StringIO() as fh:
                fh.write(xmlhead)
                for j in range(limit):
                    i = base + j                
                    if i >= len(files):
                        break
                    fname = files[i]
                    #LOGGER.debug('(%06d/%06d) Leggo il post n, \'%d\' (pack %03d) (%s)' % (
                    #    i, len(files), j, n, fname.name()))
                    fval = fname.open('r',encoding='utf-8').read()
                    fh.write(fval)
                fh.write(self.XMLFOOTER)
                blocks.append(fh.getvalue().encode('utf-8'))
        # Actually write files
        for n, block in enumerate(blocks):
            with open(genfile % n,'wb') as fo: 
                fo.write(block)
    
    def dump_db_into_xml_files(self, outfile, db, subj=None, _glob=None, force=False, limit=2500):
        exported, not_exported = self.dumps()
        xmlhead = self.XMLHEADER % self.namespaces
        numfiles = len(exported)
        numpacks = int(len(exported) / limit) + 1
        self.delete_files(outfile)
        genfile = re.sub(r'\.xml','%03d.xml',outfile)
        genfiled = re.sub(r'\.xml','<n>.xml',outfile)
        print("Save %d files in %d packs into %s" % (numfiles, numpacks, genfiled))
        n = -1
        blocks = []
        # Compose files in blocks
        for n in range(numpacks):
            base = n * limit
            with StringIO() as fh:
                fh.write(xmlhead)
                for j in range(limit):
                    i = base + j                
                    if i >= len(exported):
                        break
                    kid,docid,post = exported[i]
                    LOGGER.debug('(%06d/%06d) Leggo il post n, \'%d\' (pack %03d) (%s)' % (
                        i, len(exported), j, n, docid))
                    fh.write(post)
                fh.write(self.XMLFOOTER)
                blocks.append(fh.getvalue().encode('utf-8'))
        # Actually write files
        for n,block in enumerate(blocks):
            with open(genfile % n,'wb') as fo: 
                fo.write(block)

    def parallel_write_chunk(self,n,fname,exported,base,genfile,limit):
        xmlhead = self.XMLHEADER % self.namespaces
        with StringIO() as fh:
            fh.write(xmlhead)
            with Progress(range(limit),10,pre="C"+str(n)) as progress:
                for j in range(limit):
                    i = base + j
                    if i >= len(exported):
                        break
                    kid,docid,post = exported[i]
                    fh.write(post)
                    progress.notify(j)
            fh.write(self.XMLFOOTER)
            block = fh.getvalue().encode('utf-8')
        with open(fname,'wb') as fo:
            fo.write(block)
        size = getsize(fname)
        LOGGER.info('chunk %d ended (size=%d)' % (n,base))
        return size

    def parallel_write_els(self,exported,numpacks,genfile,limit):
        for n in range(numpacks):
            self.parallel_write_chunk(n,genfile % n,exported,n * limit,genfile,limit)
                
    
    def dump_els_into_xml_files(self, outfile, subj=None, _glob=None, force=False, limit=2500):
        exported, not_exported = self.dumps()
        xmlhead = self.XMLHEADER % self.namespaces
        numfiles = len(exported)
        numpacks = int(len(exported) / limit) + 1
        self.delete_files(outfile)
        genfile = re.sub(r'\.xml','%03d.xml',outfile)
        genfiled = re.sub(r'\.xml','<n>.xml',outfile)
        print("Save %d files in %d packs into %s" % (numfiles, numpacks, genfiled))
        n = -1
        blocks = []
        # Compose files in blocks
        self.parallel_write_els(exported, numpacks,genfile, limit)
        return exported, not_exported

    # def dump_els_into_xml_files(self, outfile, subj=None, _glob=None, force=False, limit=2500):
    #     exported, not_exported = self.dumps()
    #     xmlhead = self.XMLHEADER % self.namespaces
    #     numfiles = len(exported)
    #     numpacks = int(len(exported) / limit) + 1
    #     self.delete_files(outfile)
    #     genfile = re.sub(r'\.xml','%03d.xml',outfile)
    #     genfiled = re.sub(r'\.xml','<n>.xml',outfile)
    #     print("Save %d files in %d packs into %s" % (numfiles, numpacks, genfiled))
    #     n = -1
    #     blocks = []
    #     # Compose files in blocks
    #     with Progress(range(len(exported)),100) as progress:
    #         for n in range(numpacks):
    #             base = n * limit
    #             with StringIO() as fh:
    #                 fh.write(xmlhead)
    #                 for j in range(limit):
    #                     i = base + j                
    #                     if i >= len(exported):
    #                         break
    #                     kid,docid,post = exported[i]
    #                     #LOGGER.debug('(%06d/%06d) Leggo il post n, \'%d\' (pack %03d) (%s)' % (
    #                     #    i, len(exported), j, n, docid))
    #                     fh.write(post)
    #                     progress.notify(i)
    #                 fh.write(self.XMLFOOTER)
    #                 fh.getvalue().encode('utf-8')
    #                 blocks.append(block)
    #                 with open(genfile % n,'wb') as fo:
    #                     fo.write(block)

        # Actually write files
        # for n,block in enumerate(blocks):
        #    with open(genfile % n,'wb') as fo: 
        #        fo.write(block)


    def delete_files(self,fname):
        outfile = Path(fname)
        parent = outfile.parent
        name = outfile.name
        starname = re.sub(r'\.xml$','*.xml',name)
        # print(parent,starname)
        files = list(parent.glob(starname))
        for file in files:
            print("Deleting %s" % file.name)
            file.unlink()
