
import subprocess
from pathlib import Path

import mysql.connector
import mysql.connector.cursor as mc
from mysql.connector.errors import *

XCursor = mc.MySQLCursor
XCursorDict = mc.MySQLCursorDict



class Cursor(object):

    def __init__(self,
                 host, user,
                 passwd, dbname,
                 port,
                 cursorclass=XCursor,
                 driver=mysql.connector,
                 ):

        self.cursorclass = cursorclass
        self.host = host
        self.user = user
        self.port = port
        self.passwd = passwd
        self.dbname = dbname
        self.driver = driver
        try:
            self.connection = self.driver.connect(
                user=user, passwd=passwd,
                db=dbname, port=port, host=host)
        except DatabaseError as exc:
            print(exc)
            raise
        self.cursor = self.connection.cursor(dictionary=True)

    def __iter__(self):
        for item in self.cursor:
            yield item

    def __enter__(self):
        return self.cursor

    def __exit__(self, ext_type, exc_value, traceback):
        self.cursor.close()
        if isinstance(exc_value, Exception):
            self.connection.rollback()
        else:
            self.connection.commit()
        self.connection.close()

    def execute(self, sql, *args):
        self.cursor(sql, *args)



def mysql_drop_all_tables(**parms):
    tables = []
    with Cursor(**parms) as cursor:
        cursor.execute("SHOW tables")
        for n in cursor:
            for k,v in n.items():
                tables.append(v)
        for table in tables:
            cursor.execute("DROP TABLE "+table)


def mysql_send_file(fname,**parms):
    fpath = Path(__file__)
    sqlfile = fpath.parent / "resetdb.sql"
    if not sqlfile.exists():
        raise ValueError("File resetdb.sql not exists")
    args = ('/Applications/MAMP/Library/bin/mysql',
                    '-h'+parms['host'],
                    '-u'+parms['user'],
                    '-p'+parms['passwd'],
                    '-P'+str(parms['port']),
                    parms['dbname'])
    subprocess.call(args,stdin=sqlfile.open('r'))

class MysqlDB(object):
    def __init__(self, **parms):
        self.parms = parms
        if 'url' in parms:
            url = self.parms['url']
            del self.parms['url']
            from sqlalchemy.engine.url import make_url
            url = make_url(url)
            self.parms.update( {
                'user': url.username,
                'passwd': url.password, 
                'host': url.host, 
                'port': url.port, 
                'dbname': url.database,
            } )


    @property
    def tables(self):
        tables = []
        with Cursor(**self.parms) as cursor:
            cursor.execute("SHOW tables")
            for n in cursor:
                for k,v in n.items():
                    tables.append(v)
        return tables

    def table_count_rows(self,table):
        with Cursor(**self.parms) as cursor:
            cursor.execute("SELECT COUNT(*) AS CNT from %s" % (table))
            return cursor.fetchone()['CNT']

    def info(self,also):
        tables = []
        for table in self.tables:
            tables.append((table, self.table_count_rows(table)))
        import functools, operator


        maxl = functools.reduce(max, [ len(x) for (x,y) in tables])
        if 'tables' in also:
            for (x,y) in tables:
                print("%*s | %+7s" %(maxl,x,y))
        totals = functools.reduce(operator.add, [ y for (x,y) in tables])
        if len(also)>=0:
            print('%*s | ' %(maxl,'TOTALS:'),end='')
        print(totals)

    def sql(self, sql):
        results = []
        with Cursor(**self.parms) as cursor:
            cursor.execute(sql)
            for res in cursor:
                results.append(res)
        return results

    def create_db(self, name):
        self.sql("""CREATE DATABASE IF NOT EXISTS `%s` CHARACTER SET utf8 COLLATE utf8_unicode_ci""" % name)

    def drop_db(self, name):
        self.sql("""DROP DATABASE IF  EXISTS `%s` """ % name)

    def exists(self):
        try:
            return len(self.sql("""SHOW TABLES"""))==0
        except ProgrammingError:
            return False
