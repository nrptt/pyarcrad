"""Python package for Archivio Radicale!"""

__version__ = '0.5'

import html
import itertools
import json
import logging
import random
import re
import sqlite3
import sys
import time
from collections import UserDict, defaultdict, namedtuple
from datetime import datetime
from io import BytesIO, StringIO
from os.path import dirname, exists, join
from pprint import pformat, pprint
from string import Template

import distance
import IPython
import loremipsum
import rdflib
import wikipedia
import xmltodict
from rdflib import BNode, Graph, Literal, URIRef

LOGGER = logging.getLogger(__name__)

class config:
    HOST = 'localhost'
    USER = 'root'
    PASS = 'root'
    PORT = 8889
    MYDB = 'arcrad_cd'

# 040 x7t6j8f8f2


class Wikipedia(object):

    def __init__(self, lang='it'):
        wikipedia.set_lang(lang)

    def first_result(self, searchterm):
        result = wikipedia.search(searchterm, results=1)
        if len(result) > 0:
            return result.pop(0)


class MediaWiki(object):

    def __init__(self):
        pass

    def load(self):
        pass

    def save(self):
        pass


class Backend(object):

    def __init__(self, resource):
        self.resource = resource

    def __enter__(self):
        pass

    def __exit__(self):
        pass


def dbpediaquery():
    from SPARQLWrapper import SPARQLWrapper, JSON

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?label
    WHERE { <http://dbpedia.org/resource/Gaetano_Salvemini> rdfs:label ?label }
    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        print(result["label"]["value"].encode('utf-8'))



# for i in range(1000):
#      with Cursor() as cursor:
#         cursor.execute("SELECT * FROM document WHERE ID = %d" % i)
#         c = cursor.fetchone()
#         print(i,len(c))


# with Cursor() as cursor:
#     cursor.execute("SELECT * FROM document WHERE ID = %d" % 1)
#     print cursor.fetchone()

# with Cursor() as cursor:
#     cursor.execute("SELECT DISTINCT Lingue from document ")
#     lingue = cursor.fetchall()



def understand_document(html_document):
    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(html_document), parser)
    result = etree.tostring(tree.getroot(),
                            pretty_print=True,
                            method="html")
    print(result)






ArchivioRadicale = (
    ('ARDB', 0, 0),
    ('EXAG', 0, 0),
    ('MWDB1', 0, 0),
    ('MITT', 0, 0),
    ('RADIT', 0, 0),
)

ArchivioRadicaleP = (
    ('ARDB', 0, 100),
    ('EXAG', 0, 100),
    ('MITT', 0, 100),
    ('RADIT', 0, 100),
)

Sources = {
    'EXAG': {        
    }
}

# with MediaWiki() as wiki:
#     with Archivio1955() as backend:
#         with ArchivioDB(backend) as archive:
#             for post in archive:
#                 archive.upload(post,wiki)


def s3_execute(DBNAME, sql, *args, **kwargs):
    results = kwargs.get('results', False)
    with sqlite3.connect(DBNAME) as conn:
        cursor = conn.cursor()
        try:
            if len(args) == 0:
                cursor.execute(sql)
            else:
                cursor.execute(sql, args)
            if results:
                return cursor.fetchall()
        except OperationalError:
            pass


class NER_from_Wikipedia:

    def run(self):
        wiki = Wikipedia('it')
        DBNAME = 'wpedia.db'
        try:
            os.unlink(DBNAME)
        except:
            pass
        s3_execute(DBNAME, """CREATE TABLE RESOURCES(NAME TEXT NOT NULL,ANAME TEXT NOT NULL, WNAME TEXT NOT NULL,D1 FLOAT NOT NULL, D2 FLOAT NOT NULL);""")
        with Cursor() as cursor:
            cursor.execute("""SELECT DISTINCT Text FROM author ORDER BY Text""")
            names = cursor.fetchall()
            print("Names are", len(names))
            for j, name in enumerate(names):
                aname = name[0]
                aname = aname.title()
                if ' ' in aname:
                    (second, first) = re.split(r' ', aname, maxsplit=1)
                    aname = first + " " + second
                res = wiki.first_result(aname)
                if res:
                    D1 = distance.nlevenshtein(aname, res, method=2)
                    D2 = distance.nlevenshtein(aname, res, method=2)
                    s3_execute(DBNAME, "INSERT INTO RESOURCES(NAME,ANAME,WNAME,D1,D2) VALUES (?,?,?,?,?)", name[
                               0], aname, res, D1, D2)
                    print('%2.2f' % (float(j) / len(names)))

    def load(self, level=.15):
        DBNAME = 'wpedia.db'
        res = s3_execute(DBNAME, "SELECT * FROM RESOURCES", results=True)
        return list(filter(lambda x: x[3] < level or x[4] < level, res))

logging.basicConfig(level=logging.DEBUG)

# print("ERR",name[0].encode('utf-8'))

# NER_from_Wikipedia().run()
# res = NER_from_Wikipedia().load()

# mediawiki = {
#     'siteinfo' = {
#         'sitename' : None,
#         'bname' : None,
#         'base' : None,
#         'generator': None,
#         'case' : None,
#         'namespaces': {
#             'namespace': None
#         },
#     },
#     'page' = {
#         'title' : None,
#         'ns' :  None,
#         'id' : None,
#         'revision': {
#             'id': None,
#             'parentid': None,
#             'timestamp': None,
#             'contributor':
#         }
#     }
# }
