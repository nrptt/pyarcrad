import datetime
import logging
import os
import os.path
import pickle
import re
import time
from collections import defaultdict
from pathlib import Path
from pprint import pprint

import rdflib.query
import xmltodict
from rdflib import Graph, Literal, URIRef, plugin
from rdflib.store import NO_STORE, VALID_STORE, Store

from . import LOGGER
from .cursor import Cursor, MysqlDB
from .exporter import KwCorpusExporter
from .utils import (PRNTT, PostInfo, Progress, Timer, add_triples_to_graph, setup_parms,
                    triples_to_dict, triples_to_graph, set_triples_to_graph)

AREE = {
    "ARCEMM": (1, "Archivio Emma Bonino commissario UE"),
    "ARCFED": (2, "Archivio federalismo"),
    "ARCINT": (3, "Archivio interventi PE"),
    "ARCLIS": (4, "Archivio Lista Pannella Riformatori"),
    "ARCMAY": (5, "Archivio Mayday"),
    "ARCNES": (6, "Archivio Nessuno Tocchi Caino"),
    "ARCONU": (7, "Archivio ONU"),
    "ARCPPR": (8, "Archivio parlamentare PR"),
    "ARCPE": (9, "Archivio PE"),
    "ARCPOL": (10, "Archivio politica internazionale"),
    "ARCSEG": (11, "Archivio segreteria PR"),
    "ARCPR": (12, "Archivio Partito radicale"),
    "CONANT": (13, "Conferenza Antimilitarismo"),
    "CONARP": (14, "Conferenza Arpa"),
    "CONDAN": (15, "Conferenza Danubio"),
    "CONDRO": (16, "Conferenza droga"),
    "CONEMM": (17, "Conferenza Emma Bonino"),
    "CONERA": (18, "Conferenza ERA"),
    "CONFED": (19, "Conferenza Federalismo"),
    "CONGIU": (20, "Conferenza Giunta"),
    "CONHAN": (21, "Conferenza Hands off Cain"),
    "CONIPN": (22, "Conferenza Il partito nuovo"),
    "CONLIN": (23, "Conferenza Lingua internazionale Fundapax"),
    "CONMOV": (24, "Conferenza Movimento club Pannella"),
    "CONNON": (25, "Conferenza Non c'è pace senza giustizia"),
    "CONPOO": (26, "Conferenza Pool antiproibizionista"),
    "CONREF": (27, "Conferenza Referendum"),
    "CONRIV": (30, "Conferenza Rivoluzione liberale"),
    "CONSEG": (31, "Conferenza Segreteria CORA"),
    "CONTIB": (32, "Conferenza Tibet"),
    "CONTRA": (33, "Conferenza Transnational"),
    "CONTRI": (34, "Conferenza Tribunale internazionale"),
    "CONPAR": (35, "Conferenza Partito radicale"),
    "NOTADC": (36, "Notizie ADC CORA Fax"),
    "NOTCOR": (37, "Notizie CORA"),
    "NOTEMM": (38, "Notizie Emma Bonino"),
    "NOTESP": (39, "Notizie Esperanto"),
    "NOTPAN": (40, "Notizie lista Pannella"),
    "NOTRAD": (41, "Notizie Radicali"),
    "NOTTIB": (42, "Notizie Tibet"),
    "NOTTRA": (43, "Notizie Transnational Fax"),
}

AREE1 = {y[0]: x for x, y in AREE.items()}

# 8<------------------------------------------------------------ TO CUT

def ar_arlang(text):
    return re.sub(r'^\D+\d+(\D+)\.HTM$', '\\1', text),


def ar_arscheda(text):
    return re.sub(r'^\D+(\d+)\D+$', '\\1', text),


def ar_arid(text):
    label = re.sub(r'^(\D+)\d+\D+$', '\\1', text)
    if label in AREE:
        return AREE[label][0],
    else:
        return "0",


def ar_arlabel(text):
    return re.sub(r'^(\D+)\d+\D+$', '\\1', text),


def ar_arname(text):
    label = re.sub(r'^(\D+)\d+\D+$', '\\1', text)
    if label in AREE:
        return AREE[label][1],
    else:
        return "Sconosciuta " + label,


def ar_split(keystring):
    return list(filter(lambda x: len(x) > 1, re.split(r',', keystring.upper())))


def exa_split_keywords(keystring):
    return list(filter(lambda x: len(x) > 1, re.split(r':', keystring.upper())))


def exa_date(date):
    import pdb; pdb.set_trace()
    return date.isoformat(),


def mitt_date(date):
    return time.strftime("%Y-%m-%dT%H:%M:%S",time.gmtime(date)),


def exa_upper(string):
    return string.upper(),


def exa_archive(idcms):
    return AREE1[idcms],

def mwdb_attributes(attrs):
    attrs = re.sub('&egrave;','&#232;',attrs)
    attrs = re.sub('&agrave;','&#224;',attrs)
    attrs = re.sub('&igrave;','&#236;',attrs)
    info = xmltodict.parse(attrs, process_namespaces=True)
    return info['SCHEDA'].items()

class Database(object):
    """Il database restituisce i dati così come registrati all'interno della base dati 

    """
    SOURCE = None
    HOST = '127.0.0.1'
    USER = 'root'
    PASS = 'root'
    PORT = 8889
    MYDB = None

    def __init__(self, **kwargs):
        self.cursor = None
        self._triples = None
        self._graph = None
        self.pid = None

    @property
    def info(self):
        return triples_to_dict(self.get_triples())

    @property
    def triples(self):
        return self.get_triples()

    @property
    def graph(self):
        if self._graph:
            return self._graph
        triples = self.get_triples()
        graph = triples_to_graph(triples)
        self._graph = graph
        return graph

    def get(self, post_id):
        """Dato una chiave di accesso ottiene un dict con molteplici chiavi

        In particolare:
        SOURCE
        """
        self.pid = post_id
        if self.SQL_GET_DOC:
            self.cursor = Cursor(self.HOST, self.USER, 
                                 self.PASS, self.MYDB, 
                                 self.PORT)
            with self.cursor as cursor:
                cursor.execute(self.SQL_GET_DOC % post_id)
                doc = cursor.fetchone()
                if not doc:
                    raise IOError('doc not found n: %s' % post_id)
                doc = PostInfo(**doc)
                doc['_SOURCE'] = self.SOURCE
                return doc
        else:
            raise IOError('SQL_GET_DOC error')

    def ids(self, first=None):
        """Restituisce l'elenco delle chiavi disponibili nello store

        TODO: c'è una differenza nel funzionamento se first>0 o <0
        nel primo caso prende gli elementi il cui id è maggiore di first
        nel secondo caso gli ultimi first elementi. 
        """
        if self.SQL_GET_IDS:
            self.cursor = Cursor(self.HOST, self.USER, 
                                 self.PASS, self.MYDB, 
                                 self.PORT)
            with self.cursor as cursor:
                cursor.execute(self.SQL_GET_IDS)
                doc = cursor.fetchall()
                doc = list(map(lambda x: x['PID'], doc))
                if first is not None and first > 0:
                    doc = list(filter(
                        lambda x: True 
                        if first is None else x >= first, 
                        doc))
                elif first is not None and first < 0:
                    doc = doc[:-first]
                return doc
        else:
            raise IOError('SQL_GET_IDS error')

    def get_triples(self, post_id=None):
        """Restituisce le triplette della scheda nella base dati

        """
        if post_id is None:
            post_id = self.pid
        if post_id is None:
            IOError('post_id mismatch')
        if self.SQL_GET_TRIPLES:
            if self._triples:
                return self._triples
            triples = []
            for sql, split in self.SQL_GET_TRIPLES:
                self.cursor = Cursor(self.HOST, self.USER, 
                                     self.PASS, self.MYDB, 
                                     self.PORT)
                with self.cursor as cursor:
                    try:
                        cursor.execute(sql % post_id)
                    except TypeError as exc:
                        LOGGER.error("SQL ERROR: %s <- %s" % (sql,post_id))
                        continue
                    doc = cursor.fetchall()
                    if not doc:
                        triple = re.sub(r"^.* '([^']+)' AS `KEY`.*$",'\\1',sql)
                        LOGGER.info('Triple %s not found n: %s' % (triple,post_id))
                        continue
                    for r in doc:
                        values = [r['VALUE'], ]
                        if split:
                            values = split(r['VALUE'])
                        docid = self.get_docid(r['ID'])
                        for value in values:
                            triples.append((docid, r['KEY'], value))
            self._triples = triples
            return triples
        else:
            print(sql)
            raise IOError('SQL_GET_DOC error')

    def get_docid(self, pid):
        return "%s:%s" % (self.SOURCE, pid)

    def range(self, start, stop):
        ids = self.ids()
        if start is None:
            start = 0
        if stop is None:
            stop = 0
        if start is 0 and stop is 0:
            return ids
        if start == 0:
            start = ids[0]
        elif start < 0:
            ids = ids[-start:]
        else:
            ids = list(filter(lambda x: x >= start, ids))
        if stop == 0:
            stop = ids[-1]
        elif stop < 0:
            ids = ids[:stop]
        elif stop < start:
            ids = ids[:stop]
        else:
            ids = list(filter(lambda x: x <= stop, ids))
        return ids


class MWDB1_DB(Database):
    SOURCE = "MWDB1"
    MYDB = 'arcrad_mittwoch'
    SQL_GET_DOC = """SELECT D.id AS PID D.doc_title AS TITLE, 
                            D.doc_text AS TEXT
                            FROM cms_document D, 
                                 cms_cat_doc CD, 
                                 cms_list L
                            WHERE D.id = CD.iddocument AND 
                                  CD.idcms = L.id AND D.id=%d"""
    SQL_GET_IDS = "SELECT DISTINCT D.id AS PID FROM cms_document D, cms_cat_doc CD, cms_list L WHERE D.id = CD.iddocument AND CD.idcms = L.id "
    SQL_GET_TRIPLES = (
        ("SELECT id AS ID, 'OLDURL' AS `KEY`, CONCAT('http://old.radicali.it/view.php?id=', id) AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'TEXT' AS `KEY`, doc_text AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'ARCPID' AS `KEY`, id AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'TITLE' AS `KEY`, doc_title AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT iddocument AS ID, 'KEY' AS `KEY`, keydoc AS VALUE FROM cms_keydoc KD, cms_keys K WHERE K.id = KD.idkey AND iddocument=%d ", None),
        ("SELECT id AS ID, 'DATE' AS `KEY`, doc_date AS VALUE FROM cms_document WHERE id=%d ", exa_date),
        ("SELECT id AS ID, 'AUTHOR' AS `KEY`, doc_author AS VALUE FROM cms_document WHERE id=%d ", exa_upper),
        ("SELECT id AS ID, 'LANG' AS `KEY`, 'IT' AS VALUE FROM cms_document WHERE id=%d ", exa_upper),
        ("SELECT id AS ID, 'ORIGIN' AS `KEY`, doc_origin AS VALUE FROM cms_document WHERE id=%d ", exa_upper),
        ("SELECT id AS ID, 'ATTRIBUTES' AS `KEY`, doc_attributes AS VALUE FROM cms_document WHERE id=%d ", mwdb_attributes),
        ("SELECT iddocument AS ID, 'ARCID' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", None),
        ("SELECT iddocument AS ID, 'ARCLABEL' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", exa_archive),
        ("SELECT iddocument AS ID, 'ARCNAME' AS `KEY`, name AS VALUE FROM cms_cat_doc D, cms_list L WHERE D.idcms = L.id AND iddocument=%d ", None),
    )

class DRUPAL_DB(Database):
    SOURCE = None
    MYDB = None
    SQL_GET_DOC = """SELECT D.nid AS PID, D.title AS TITLE,
                            R.body AS TEXT
                            FROM node D, node_revisions R 
                            WHERE D.nid = R.nid AND  D.vid = R.vid
                            AND D.type = 'document' 
                            AND D.nid = %d"""
    SQL_GET_IDS = "SELECT DISTINCT D.nid AS PID FROM node D ORDER BY D.nid"
    SQL_GET_TRIPLES = (
        ("SELECT N.nid AS ID, 'ABSTRACT' AS `KEY`, R.teaser AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid", None),
        ("SELECT N.nid AS ID, 'TEXT' AS `KEY`, R.body AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
#        ("SELECT id AS ID, 'ARCPID' AS `KEY`, id AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'TITLE' AS `KEY`, R.title AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'KEY' AS `KEY`, D.name AS VALUE FROM node_revisions R, node N, term_node T, term_data D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = T.nid AND N.vid = T.vid AND T.tid = D.tid ", None),
        ("SELECT N.nid AS ID, 'DOCPUBLISHED' AS `KEY`, D.doc_published AS VALUE FROM node_revisions R, node N, document D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = D.nid ", exa_date),
        ("SELECT N.nid AS ID, 'SUBTYPE' AS `KEY`, D.doc_subtype AS VALUE FROM node_revisions R, node N, document D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = D.nid ", None),
        ("SELECT N.nid AS ID, 'DATE' AS `KEY`, timestamp AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'CREATED' AS `KEY`, created AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'CHANGED' AS `KEY`, changed AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'NREVISIONS' AS `KEY`, COUNT(R.vid) AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid  ", None),
        ("SELECT N.nid AS ID, 'REVISION' AS `KEY`, R.vid AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid  ", None),
        ("SELECT N.nid AS ID, 'LANG' AS `KEY`, N.language AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", exa_upper),
        ("SELECT N.nid AS ID, 'FILE' AS `KEY`, CONCAT(F.filemime,':',F.filename,':',U.description) AS VALUE FROM node_revisions R, node N, files F, upload U WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = U.nid AND N.vid = U.vid AND U.fid = F.fid ", None),
#        ("SELECT N.nid AS ID, 'ORIGIN' AS `KEY`, doc_origin AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", exa_upper),
#        ("SELECT N.nid AS ID, 'ATTRIBUTES' AS `KEY`, doc_attributes AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
#        ("SELECT N.nid AS ID, 'ARCID' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", None),
#        ("SELECT N.nid AS ID, 'ARCLABEL' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", exa_archive),
#        ("SELECT N.nid AS ID, 'ARCNAME' AS `KEY`, name AS VALUE FROM cms_cat_doc D, cms_list L WHERE D.idcms = L.id AND iddocument=%d ", None),
    )
    

class MITT_DB(DRUPAL_DB):
    SOURCE = "MITT"
    MYDB = 'arcrad_mittwoch'
    SQL_GET_TRIPLES = (
        ("SELECT N.nid AS ID, 'ABSTRACT' AS `KEY`, R.teaser AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid", None),
        ("SELECT N.nid AS ID, 'TEXT' AS `KEY`, R.body AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
#        ("SELECT id AS ID, 'ARCPID' AS `KEY`, id AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'TITLE' AS `KEY`, R.title AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'KEY' AS `KEY`, D.name AS VALUE FROM node_revisions R, node N, term_node T, term_data D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = T.nid AND N.vid = T.vid AND T.tid = D.tid ", None),
        ("SELECT N.nid AS ID, 'DOCPUBLISHED' AS `KEY`, D.doc_published AS VALUE FROM node_revisions R, node N, document D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = D.nid ", exa_date),
        ("SELECT N.nid AS ID, 'SUBTYPE' AS `KEY`, D.doc_subtype AS VALUE FROM node_revisions R, node N, document D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = D.nid ", None),
        ("SELECT N.nid AS ID, 'DATE' AS `KEY`, timestamp AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'CREATED' AS `KEY`, created AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'CHANGED' AS `KEY`, changed AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'NREVISIONS' AS `KEY`, COUNT(R.vid) AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid  ", None),
        ("SELECT N.nid AS ID, 'REVISION' AS `KEY`, R.vid AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid  ", None),
        ("SELECT N.nid AS ID, 'LANG' AS `KEY`, N.language AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", exa_upper),
        ("SELECT N.nid AS ID, 'FILE' AS `KEY`, CONCAT(F.filemime,':',F.filename,':',U.description) AS VALUE FROM node_revisions R, node N, files F, upload U WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = U.nid AND N.vid = U.vid AND U.fid = F.fid ", None),
#        ("SELECT N.nid AS ID, 'ORIGIN' AS `KEY`, doc_origin AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", exa_upper),
#        ("SELECT N.nid AS ID, 'ATTRIBUTES' AS `KEY`, doc_attributes AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
#        ("SELECT N.nid AS ID, 'ARCID' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", None),
#        ("SELECT N.nid AS ID, 'ARCLABEL' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", exa_archive),
#        ("SELECT N.nid AS ID, 'ARCNAME' AS `KEY`, name AS VALUE FROM cms_cat_doc D, cms_list L WHERE D.idcms = L.id AND iddocument=%d ", None),
    )

class RADIT_DB(DRUPAL_DB):
    SOURCE = "RADIT"
    MYDB = 'arcrad_radicali'
    SQL_GET_DOC = """SELECT D.nid AS PID, D.title AS TITLE,
                            R.body AS TEXT
                            FROM node D, node_revisions R 
                            WHERE D.nid = R.nid AND  D.vid = R.vid
                            AND D.nid = %d"""
    SQL_GET_IDS = "SELECT DISTINCT D.nid AS PID FROM node D ORDER BY D.nid"
    SQL_GET_TRIPLES = (
        ("SELECT N.nid AS ID, 'ABSTRACT' AS `KEY`, R.teaser AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid", None),
        ("SELECT N.nid AS ID, 'TEXT' AS `KEY`, R.body AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'TYPE' AS `KEY`, N.type AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'TITLE' AS `KEY`, R.title AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", None),
        ("SELECT N.nid AS ID, 'KEY' AS `KEY`, D.name AS VALUE FROM node_revisions R, node N, term_node T, term_data D WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = T.nid AND N.vid = T.vid AND T.tid = D.tid ", None),
        ("SELECT N.nid AS ID, 'DATE' AS `KEY`, timestamp AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'CREATED' AS `KEY`, created AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'CHANGED' AS `KEY`, changed AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", mitt_date),
        ("SELECT N.nid AS ID, 'NREVISIONS' AS `KEY`, COUNT(R.vid) AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid  ", None),
        ("SELECT N.nid AS ID, 'REVISION' AS `KEY`, R.vid AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid  ", None),
        ("SELECT N.nid AS ID, 'LANG' AS `KEY`, N.language AS VALUE FROM node_revisions R, node N WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid ", exa_upper),
        ("SELECT N.nid AS ID, 'FILE' AS `KEY`, CONCAT(F.filemime,':',F.filename,':',U.description) AS VALUE FROM node_revisions R, node N, files F, upload U WHERE N.nid=%d AND N.nid = R.nid AND N.vid = R.vid AND N.nid = U.nid AND N.vid = U.vid AND U.fid = F.fid ", None),
        )

class ARCD_DB(Database):
    """ARCD è il database costruito a partire dal CD-ROM dell'Archivio Radicale distribuito alla fine degli anni '90 da Radio Radicale

    Il DB era presente sul CD-ROM in formato Access (MDB) ed è stato preliminarmente trasformato in SQL per il backend MySQL.

    Sono presenti le seguenti tabelle:

    - author: elenco degli autori (2503 righe). Il campo ID contiene un identificativo numerico incrementale, 
    il campo Text contiene il nome dell'autore in lettere maiuscole, se persona nel formato COGNOME NOME
    - author_count: lega l'identificativo dell'autore a quello del documento (informazione ridondante)
    - count: lega l'identificativo della parola (tabella word) e quello del documento con il relativo conteggio
    - document: tabella dei documenti 
    - occurrence: tabella vuota
    - word: tabelle delle parole presenti nei documenti con l'indicazione se devono essere indicizzate o se sono keywords




    """
    SOURCE = "ARCD"
    MYDB = "arcrad_cd"
    SQL_GET_DOC = """SELECT ID AS PID,LEFT(Path,LENGTH(Path)-4) AS LABEL, Titolo AS TITLE, 
                            Autore AS AUTHOR, Data AS DATE, Text AS TEXT, 
                            "" AS `KEYS`, "IT" AS LANG, 
                            LEFT(Path,LENGTH(Path)-4) AS ARCID, LEFT(Path,LENGTH(Path)-4) AS ARCNAME 
                            FROM document WHERE ID=%d"""
    SQL_GET_IDS = "SELECT DISTINCT ID AS PID FROM document"
    SQL_GET_TRIPLES = (
        ("SELECT id AS ID, 'TEXT' AS `KEY`, Text AS VALUE FROM document WHERE ID=%d", None),
        ("SELECT id AS ID, 'ARCPID' AS `KEY`, Path AS VALUE FROM document WHERE ID=%d", ar_arscheda),
        ("SELECT id AS ID, 'TITLE' AS `KEY`, Titolo AS VALUE FROM document WHERE ID=%d", None),
        #        ("SELECT id AS ID, 'KEY' AS `KEY`, '<$CHIAVI>' AS VALUE FROM document WHERE ID=%d", None),
        ("SELECT id AS ID, 'DATE' AS `KEY`, Data AS VALUE FROM document WHERE ID=%d", exa_date),
        ("SELECT id AS ID, 'AUTHOR' AS `KEY`, Autore AS VALUE FROM document WHERE ID=%d ", exa_upper),
        ("SELECT id AS ID, 'LANG' AS `KEY`,  Path AS VALUE FROM document WHERE ID=%d ", ar_arlang),
        ("SELECT id AS ID, 'LANGS' AS `KEY`,  Lingue AS VALUE FROM document WHERE ID=%d ", ar_split),
        ("SELECT id AS ID, 'ARCID' AS `KEY`, Path AS VALUE FROM document WHERE ID=%d", ar_arid),
        ("SELECT id AS ID, 'ARCLABEL' AS `KEY`, Path AS VALUE FROM document WHERE id=%d ", ar_arlabel),
        ("SELECT id AS ID, 'ARCNAME' AS `KEY`, Path AS VALUE FROM document WHERE id=%d ", ar_arname),
    )


class EXAG_DB(Database):
    SOURCE = "EXAG"
    MYDB = 'arcrad_exagora'
    SQL_GET_DOC = """SELECT D.id AS PID, D.idscheda AS LABEL, D.doc_title AS TITLE, 
                            D.doc_author AS AUTHOR, D.doc_date AS DATE, D.doc_text AS TEXT, 
                            D.doc_keys AS `KEYS`, D.doc_lang AS LANG,
                            L.id AS ARCID, L.name AS ARCNAME
                            FROM cms_document D, cms_cat_doc CD, cms_list L
                            WHERE D.id = CD.iddocument AND 
                                  CD.idcms = L.id AND D.id=%d"""
    SQL_GET_IDS = "SELECT DISTINCT ID AS PID FROM cms_document"
    SQL_GET_TRIPLES = (
        ("SELECT id AS ID, 'OLDURL' AS `KEY`, CONCAT('http://old.radicali.it/search_view.php?id=', id) AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'TEXT' AS `KEY`, doc_text AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'ARCPID' AS `KEY`, idscheda AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'TITLE' AS `KEY`, doc_title AS VALUE FROM cms_document WHERE id=%d ", None),
        ("SELECT id AS ID, 'KEY' AS `KEY`, doc_keys AS VALUE FROM cms_document WHERE id=%d ", exa_split_keywords),
        ("SELECT id AS ID, 'DATE' AS `KEY`, doc_date AS VALUE FROM cms_document WHERE id=%d ", exa_date),
        ("SELECT id AS ID, 'AUTHOR' AS `KEY`, UPPER(doc_author) AS VALUE FROM cms_document WHERE id=%d ", exa_upper),
        ("SELECT id AS ID, 'LANG' AS `KEY`, doc_lang AS VALUE FROM cms_document WHERE id=%d ", exa_upper),
        ("SELECT iddocument AS ID, 'ARCID' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", None),
        ("SELECT iddocument AS ID, 'ARCLABEL' AS `KEY`, idcms AS VALUE FROM cms_cat_doc WHERE iddocument=%d ", exa_archive),
        ("SELECT iddocument AS ID, 'ARCNAME' AS `KEY`, name AS VALUE FROM cms_cat_doc D, cms_list L WHERE D.idcms = L.id AND iddocument=%d ", None),
    )


# 8< ------------------------------------------------------------

# NEW FUNCTIONS

def d_exa_date(key,value):
    if value['VALUE']:
        value['VALUE'] = value['VALUE'].isoformat()
    return ((key,value),)

def d_exa_date_from_timestamp(key,value):
    if value['VALUE']:
        value['VALUE'] = datetime.datetime.fromtimestamp(
            int(value['VALUE'])
        ).strftime("%Y-%m-%dT%H:%M:%S")
    return ((key,value),)

def d_exa_upper(key,value):
    if value['VALUE']:
        value['VALUE'] = value['VALUE'].upper()
    return ((key,value),)

def d_keys_multiple(key,value):
    results = []
    if value['VALUE']:
        if ':' in value['VALUE']:
            values = re.split(':',value['VALUE'])
            for val in values:
                if len(val)>0:
                    valout = dict(**value)
                    valout['VALUE'] = val.upper()
                    results.append((key,valout))
    return results

def d_mitt_date(key,date):
    # date['VALUE'] = time.strftime("%Y-%m-%dT%H:%M:%S",time.gmtime(date['VALUE']))
    if date['VALUE']:
        date['VALUE'] = date['VALUE'].strftime("%Y-%m-%dT%H:%M:%S")
    return ((key,date),)

class DBDump(object):
    BASEDIR = Path('~/.pyarcrad_cache').expanduser()

    SOURCES_labs = {
        'arcrad_exagora': 'EXAG',
        'arcrad_radicali': 'RADI',
        'arcrad_oldradicali': 'ORAD',
        'arcrad_mittwoch': 'MITT',
        'arcrad_cora': 'CORA',
        'arcrad_coraold': 'OCOR',
        'arcrad_coranew': 'NCOR',
    }

    SOURCE_defs = {
        'arcrad_exagora':  ( 
            ( 0, 'base',
              "PID", 
              """\
              SELECT D.id AS PID,
              'EXAG' AS SOURCE,
              CONCAT('EXAG:',D.id) AS DOCID,
              D.idscheda AS ARCPID, 
              D.doc_title AS TITLE, 
              D.doc_author AS AUTHOR, 
              D.doc_text AS TEXT, 
              D.doc_lang AS LANG,
              L.id AS ARCID, 
              L.name AS ARCNAME,
              CONCAT('http://old.radicali.it/search_view.php?id=', D.id) AS OLDURL
              FROM cms_document D, cms_cat_doc CD, cms_list L
              WHERE D.id = CD.iddocument AND 
              CD.idcms = L.id""",  None) , 
            ( 1, 
              "keys", "PID",
              """SELECT D.id AS PID,
              'KEY' AS `KEY`,
              D.doc_keys AS VALUE 
              FROM cms_document D
              """,
              d_keys_multiple),
            ( 1, 
              "date", "PID",
              """SELECT D.id AS PID,
              'DATE' AS `KEY`,
              D.doc_date AS VALUE 
              FROM cms_document D
              """,
              d_mitt_date),
        ),
        "arcrad_radicali": ( ( 0, 'base',
                     "PID", 
                     """SELECT 
                     N.nid AS PID, 
                     'RADI' AS SOURCE,
                     CONCAT('RAD:',N.nid) AS DOCID,
                     N.title AS TITLE,
                     R.body AS TEXT,
                     N.type AS `TYPE`
                     FROM node N, node_revisions R
                     WHERE N.nid = R.nid AND  N.vid = R.vid
                     """, None), 
                   ( 1, 'keys',
                     "ID", 
                     """SELECT N.nid AS ID, 
                     'KEY' AS `KEY`, 
                     D.name AS VALUE 
                     FROM node_revisions R, node N, term_node T, term_data D 
                     WHERE N.nid = R.nid AND N.vid = R.vid 
                     AND N.nid = T.nid AND N.vid = T.vid AND T.tid = D.tid """, None),
                   ( 1, 'files',
                     "ID",
                     """SELECT N.nid AS ID, 
                     'FILE' AS `KEY`, 
                     CONCAT(F.filemime,':',F.filename,':',U.description) AS VALUE 
                     FROM node_revisions R, node N, files F, upload U 
                     WHERE N.nid = R.nid AND N.vid = R.vid 
                     AND N.nid = U.nid AND N.vid = U.vid AND U.fid = F.fid """, None),
                    (1, 
                     'ABSTRACT', 
                     "ID",
                     """SELECT N.nid AS ID, 'ABSTRACT' AS `KEY`, R.teaser AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE N.nid = R.nid AND N.vid = R.vid""", None),
                    (1, 
                    'DATE', 
                     "ID",
                     """SELECT N.nid AS ID, 'DATE' AS `KEY`, timestamp AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_date_from_timestamp),
                    (1, 
                     'CREATED', 
                     "ID",
                     """SELECT N.nid AS ID, 'CREATED' AS `KEY`, created AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_date_from_timestamp),
                    (1, 
                     'CHANGED', 
                     "ID",
                     """SELECT N.nid AS ID, 'CHANGED' AS `KEY`, changed AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_date_from_timestamp),
                    (1, 
                     'NREVISIONS', 
                     "ID",
                     """SELECT N.nid AS ID, 'NREVISIONS' AS `KEY`, COUNT(R.vid) AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE  N.nid = R.nid
                     GROUP BY N.nid""", None),
                    (1, 
                     'REVISION', 
                     "ID",
                     """SELECT N.nid AS ID, 'REVISION' AS `KEY`, R.vid AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE  N.nid = R.nid""", None),
                    (1, 
                     'LANG', 
                     "ID",
                     """SELECT N.nid AS ID, 'LANG' AS `KEY`, N.language AS VALUE 
                     FROM node_revisions R, node N 
                     WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_upper),
                    (1, 
                     'FILE', 
                     "ID",
                     """SELECT N.nid AS ID, 'FILE' AS `KEY`, 
                     CONCAT(F.filemime,':',F.filename,':',U.description) AS VALUE 
                     FROM node_revisions R, node N, files F, upload U 
                     WHERE  N.nid = R.nid AND N.vid = R.vid AND N.nid = U.nid 
                     AND N.vid = U.vid AND U.fid = F.fid""", None), ),
        'arcrad_mittwoch': ( ( 0, 
                               'base',
                               "PID",
                               """SELECT D.nid AS PID, 
                               'MITT' AS SOURCE,
                               CONCAT('MITT:',D.nid) AS DOCID,
                               D.title AS TITLE,
                               R.body AS TEXT
                               FROM node D, node_revisions R 
                               WHERE D.nid = R.nid AND  D.vid = R.vid
                               AND D.type = 'document' 
                               """, None),
                             (1, 
                              'ABSTRACT', 
                              "ID",
                              """SELECT N.nid AS ID, 'ABSTRACT' AS `KEY`, R.teaser AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE N.nid = R.nid AND N.vid = R.vid""", None),
                             (1, 
                              'KEY', 
                              "ID",
                              """SELECT N.nid AS ID, 'KEY' AS `KEY`, D.name AS VALUE 
                              FROM node_revisions R, node N, term_node T, term_data D 
                              WHERE  N.nid = R.nid AND N.vid = R.vid AND N.nid = T.nid 
                              AND N.vid = T.vid AND T.tid = D.tid""", None),
                             (1, 
                              'DOCPUBLISHED', 
                              "ID",
                              """SELECT N.nid AS ID, 'DOCPUBLISHED' AS `KEY`, D.doc_published AS VALUE 
                              FROM node_revisions R, node N, document D WHERE  N.nid = R.nid 
                              AND N.vid = R.vid AND N.nid = D.nid""", d_exa_date),
                             (1, 
                              'SUBTYPE', 
                              "ID",
                              """SELECT N.nid AS ID, 'SUBTYPE' AS `KEY`, D.doc_subtype AS VALUE 
                              FROM node_revisions R, node N, document D 
                              WHERE  N.nid = R.nid AND N.vid = R.vid AND N.nid = D.nid""", None),
                             (1, 
                              'DATE', 
                              "ID",
                              """SELECT N.nid AS ID, 'DATE' AS `KEY`, timestamp AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_date_from_timestamp),
                             (1, 
                              'CREATED', 
                              "ID",
                              """SELECT N.nid AS ID, 'CREATED' AS `KEY`, created AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_date_from_timestamp),
                             (1, 
                              'CHANGED', 
                              "ID",
                              """SELECT N.nid AS ID, 'CHANGED' AS `KEY`, changed AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_date_from_timestamp),
                             (1, 
                              'NREVISIONS', 
                              "ID",
                              """SELECT N.nid AS ID, 'NREVISIONS' AS `KEY`, COUNT(R.vid) AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE  N.nid = R.nid
                              GROUP BY N.nid""", None),
                             (1, 
                              'REVISION', 
                              "ID",
                              """SELECT N.nid AS ID, 'REVISION' AS `KEY`, R.vid AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE  N.nid = R.nid""", None),
                             (1, 
                              'LANG', 
                              "ID",
                              """SELECT N.nid AS ID, 'LANG' AS `KEY`, N.language AS VALUE 
                              FROM node_revisions R, node N 
                              WHERE  N.nid = R.nid AND N.vid = R.vid""", d_exa_upper),
                             (1, 
                              'FILE', 
                              "ID",
                              """SELECT N.nid AS ID, 'FILE' AS `KEY`, 
                              CONCAT(F.filemime,':',F.filename,':',U.description) AS VALUE 
                              FROM node_revisions R, node N, files F, upload U 
                              WHERE  N.nid = R.nid AND N.vid = R.vid AND N.nid = U.nid 
                              AND N.vid = U.vid AND U.fid = F.fid""", None), ),
        'arcrad_oldradicali':  ( 
            ( 0, 'base',
              "PID", 
              """\
              SELECT D.id AS PID,
              D.id   AS ARCPID, 
              'ORAD' AS SOURCE,
              CONCAT('ORAD:',D.id) AS DOCID,
              D.doc_title AS TITLE, 
              D.doc_author AS AUTHOR, 
              D.doc_origin AS ORIGIN, 
              D.doc_text AS TEXT, 
              L.id AS ARCID, 
              L.name AS ARCNAME,
              CONCAT('http://old.radicali.it/search_view.php?id=', D.id) AS OLDURL
              FROM cms_document D, cms_cat_doc CD, cms_list L
              WHERE D.id = CD.iddocument AND 
              CD.idcms = L.id""",  None) , 
            ( 1, 
              "date", "PID",
              """SELECT D.id AS PID,
              'DATE' AS `KEY`,
              D.doc_date AS VALUE 
              FROM cms_document D
              """,
              d_mitt_date),
            ( 1, 
              "date_insert", "PID",
              """SELECT D.id AS PID,
              'DATE_INSERT' AS `KEY`,
              D.doc_date_insert AS VALUE 
              FROM cms_document D
              """,
              d_mitt_date),
            ( 1, 
              "date_news", "PID",
              """SELECT D.id AS PID,
              'DATE_NEWS' AS `KEY`,
              D.doc_date_news AS VALUE 
              FROM cms_document D
              """,
              d_mitt_date),
            ( 1, 
              "sectors", "PID",
              """SELECT D.id AS PID,
              'DATE_NEWS' AS `KEY`,
              D.doc_sectors AS VALUE 
              FROM cms_document D
              """,
              None),
            ( 1, 
              "categories", "PID",
              """SELECT D.id AS PID,
              'CATEGORY_TITLE' AS `KEY`,
              C.cat_title AS VALUE 
              FROM cms_document D, cms_cat_doc CD, cms_cat C
              WHERE D.id = CD.iddocument AND 
              CD.idcms = C.idcms AND CD.idcat = C.id""",  
              None), 
            ( 1, 
              "category-texts", "PID",
              """SELECT D.id AS PID,
              'CATEGORY_TEXT' AS `KEY`,
              C.cat_text AS VALUE 
              FROM cms_document D, cms_cat_doc CD, cms_cat C
              WHERE D.id = CD.iddocument AND 
              CD.idcms = C.idcms AND CD.idcat = C.id""",  
              None), 
            ( 1, 
              "keys", "PID",
              """SELECT D.id AS PID,
              'KEY' AS `KEY`,
              K.keydoc AS VALUE 
              FROM cms_document D, cms_keydoc KD, cms_keys K
              WHERE D.id = KD.iddocument AND 
              KD.idkey = K.id""",  
              None), 
            ( 1, 
              "links", "PID",
              """SELECT D.id AS PID,
              'LINK_URL' AS `KEY`,
              L.link_url AS VALUE 
              FROM cms_document D, cms_link L
              WHERE D.id = L.iddocument 
              """,  
              None), 
            ( 1, 
              "link-text", "PID",
              """SELECT D.id AS PID,
              'LINK_URL' AS `KEY`,
              L.link_text AS VALUE 
              FROM cms_document D, cms_link L
              WHERE D.id = L.iddocument 
              """,  
              None), 
        ),
        'arcrad_cora':  ( 
            ( 0, 'base',
              "PID", 
              """\
              SELECT D.sid AS PID,
              'CORA' AS SOURCE,
              CONCAT('CORA:',D.sid) AS DOCID,
              D.sid AS ARCPID, 
              D.title AS TITLE, 
              D.bodytext AS TEXT, 
              D.hometext AS ABSTRACT, 
              'IT' AS LANG,
              T.topicid AS ARCID, 
              T.topictext AS ARCNAME
              FROM cora_stories D, cora_topics T
              WHERE D.topic = T.topicid 
              """,  None) , 
            ( 1, 
              "date", "PID",
              """SELECT D.sid AS PID,
              'DATE' AS `KEY`,
              D.time AS VALUE 
              FROM cora_stories D
              """,
              d_mitt_date),
        ),
        'arcrad_coraold':  ( 
            ( 0, 'base',
              "PID", 
              """\
              SELECT D.sid AS PID,
              'CORA' AS SOURCE,
              CONCAT('CORA:',D.sid) AS DOCID,
              D.sid AS ARCPID, 
              D.title AS TITLE, 
              D.bodytext AS TEXT, 
              D.hometext AS ABSTRACT, 
              'IT' AS LANG,
              T.topicid AS ARCID, 
              T.topictext AS ARCNAME
              FROM stories D, topics T
              WHERE D.topic = T.topicid 
              """,  None) , 
            ( 1, 
              "date", "PID",
              """SELECT D.sid AS PID,
              'DATE' AS `KEY`,
              D.time AS VALUE 
              FROM stories D
              """,
              d_mitt_date),
        ),
        'arcrad_coranew':  ( 
            ( 0, 'base',
              "PID", 
              """\
              SELECT D.sid AS PID,
              'CORA' AS SOURCE,
              CONCAT('CORA:',D.sid) AS DOCID,
              D.sid AS ARCPID, 
              D.title AS TITLE, 
              D.bodytext AS TEXT, 
              D.hometext AS ABSTRACT, 
              'IT' AS LANG,
              T.topicid AS ARCID, 
              T.topictext AS ARCNAME
              FROM cora_stories D, cora_topics T
              WHERE D.topic = T.topicid 
              """,  None) , 
            ( 1, 
              "date", "PID",
              """SELECT D.sid AS PID,
              'DATE' AS `KEY`,
              D.time AS VALUE 
              FROM cora_stories D
              """,
              d_mitt_date),
        ),
        }


    def __init__(self,**parms):
        self.parms = parms
        self.tables = {}
        self._triples = []
        self._graph = None
        self.source_labs = {}

    def pickle(self, table, content, method):
        tfname = self.BASEDIR / (self.parms['dbname'] + "-" + table + ".tbl")
        if method == 'load':
            func = lambda x,y: pickle.load(x.open('rb'))
        elif method == 'dump':
            if not self.BASEDIR.exists():
                self.BASEDIR.mkdir()
            func = lambda x,y: pickle.dump(y,x.open('wb'))
        else:
            raise ValueError('method unsupported')
        if tfname.exists() or method == 'dump':
            try:
                t = func(tfname,content)
            except Exception:
                return
            return t

    def setUp(self, source, tables, reset_cache=False):
        self.source = source
        to_load = []
        for n,(kind, name, key, sql, func) in enumerate(tables):
            t = None
            if not reset_cache:
                t = self.pickle(name,None,'load')
            if t:
                self.tables[name]=t
            else:
                to_load.append((kind, name, key, sql, func))
        LOGGER.info('    Pickled: '+' '.join(sorted(self.tables.keys())))
        LOGGER.info('Not pickled: '+' '.join(sorted(map(lambda x: x[1],to_load))))
        self.LoadTables(to_load)


    def LoadTables(self, tables):
        print("Setting up")
        mdb = MysqlDB(**self.parms)
        if len(tables)==0:
            return
        N = len(tables)
        LEN = len(str(N))
        for n,(kind,name,key,sql,func) in enumerate(tables):
            print("%0*d/%0*d %s" % (LEN,n,LEN,N,name))
            sqlres = mdb.sql(sql)
            meth = lambda _k,x: ((_k,x),)
            if func:
                meth = func
            table = list()
            for res in sqlres:
                # res è un dict
                _key = res[key]
                _lines = meth(_key,res)
                table.extend(_lines)
            self.tables[name] = ( kind, table )
                # self.tables[name] = ( kind, { r[key]:meth(r) for r in res })
            self.pickle(name,self.tables[name],'dump')

    def generateTriples(self):
        for tabname,(kind,table) in self.tables.items():
            for key, info in table:
                if kind == 0:
                    for var, value in info.items():
                        self._triples.append( ("%s:%d" % (self.source,key), var, value) )
                elif kind == 1:
                    try:
                        self._triples.append( ("%s:%d" % (self.source,key), info['KEY'], info['VALUE']) )
                    except:
                        import pdb; pdb.set_trace()


    def make_graph(self, store, verbose=False):
        triples = self._triples
        graph = add_triples_to_graph(store.graph, 'arcrad', triples, verbose=verbose)
        self._graph = graph
        return store

    def info(self):
        def evaluate(n,table):
            if n==0:
                first = list(table[0][1].keys())
                return len(table)*len(first)
            if n==1:
                return len(table)
            return 0
        for name,table in sorted(self.tables.items()):
            print("%-20s | %+7s | %+9s" % (name, len(table[1]), evaluate(*table)))
    
    def load(self,db,dbout=None,force=False,reset_cache=False,verbose=False):
        defs = self.SOURCE_defs[db]
        lab = self.SOURCES_labs[db]
        self.setUp( lab,  defs , reset_cache=reset_cache)
        self.info()
        self.generateTriples()
        from .graph import SQLAlchemyTripleStore
        if not dbout:
            dbout = "out_" + db
        store = SQLAlchemyTripleStore(label=dbout,force=force,create=True)
        return self.make_graph(store)

    def convoy(self,db,dbout=None,force=False,reset_cache=False,verbose=False):
        defs = self.SOURCE_defs[db]
        lab = self.SOURCES_labs[db]
        self.setUp( lab,  defs , reset_cache=reset_cache)
        self.info()
        self.generateTriples()
        from .graph import SQLAlchemyTripleStore
        if not dbout:
            dbout = "out_" + db
        store = SQLAlchemyTripleStore(label=dbout,force=force)
        triples = self._triples
        graph = set_triples_to_graph(store.graph, 'arcrad', triples, verbose=verbose)
        self._graph = graph
        return store

def load_source(parms, db, dbout=None, force=False, reset_cache=False,verbose=False):
    ddb = DBDump(**parms)
    return ddb.load(db,dbout=dbout, force=force, reset_cache=reset_cache, verbose=verbose)

def convoy_source(parms, db, dbout=None, force=False, reset_cache=False,verbose=False):
    ddb = DBDump(**parms)
    return ddb.convoy(db,dbout=dbout, force=force, reset_cache=reset_cache, verbose=verbose)

class mpickle(object):
    
    @staticmethod
    def dump(out_bytes,f_out):
        n_bytes = 2**31
        max_bytes = 2**31 - 1
        bytes_out = pickle.dumps(out_bytes)
        fh = f_out.open('wb')
        for idx in range(0, len(bytes_out), max_bytes):
            fh.write(bytes_out[idx:idx+max_bytes])

    @staticmethod
    def load(f_in):
        n_bytes = 2**31
        max_bytes = 2**31 - 1
        bytes_in = bytearray()
        input_size = f_in.stat().st_size
        fh = f_in.open('rb')
        for _ in range(0, input_size, max_bytes):
            bytes_in += fh.read(max_bytes)
        res = pickle.loads(bytes_in)
        return res

class SpeedGraphDumper(object):
    TABLE = 'kb_30e064761f_literal_statements'
    BASEDIR = Path('~/.pyarcrad_cache').expanduser()

    def __init__(self,**parms):
        self.parms = parms
        self.tables = {}

    def pickle(self, table, content, method):
        tfname = self.BASEDIR / (self.parms['dbname'] + "-" + table + ".tbl")
        if method == 'load':
            func = lambda x,y: mpickle.load(x) # .open('rb'))
        elif method == 'dump':
            if not self.BASEDIR.exists():
                self.BASEDIR.mkdir()
            func = lambda x,y: mpickle.dump(y,x)  # .open('wb'))
        else:
            raise ValueError('method unsupported')
        if tfname.exists() or method == 'dump':
            try:
                t = func(tfname, content)
                LOGGER.info(method+"ed pickle for table "+table+".tbl in "+str(tfname))
            except Exception as exc:
                LOGGER.info("not "+method+"ed pickle for table "+table+".tbl in "+str(tfname))
                import sys
                import traceback
                traceback.print_exception(*sys.exc_info(), limit=2)
                return
            return t

    def setUp(self, reset_cache=False):
        to_load = []        
        t = None
        name = self.TABLE
        if not reset_cache:
            t = self.pickle(name,None,'load')
        if t:
            self.tables[name]=t
        else:
            to_load.append(name)
        LOGGER.info('Not pickled: '+' '.join(sorted(to_load)))
        self.LoadTables(to_load)


    def LoadTables(self, tables):
        print("Setting up")
        mdb = MysqlDB(**self.parms)
        if len(tables)==0:
            return
        N = len(tables)
        LEN = len(str(N))
        _els = {}
        for n,name in enumerate(tables):
            sql = 'SELECT * FROM ' + name
            key = 'id'
            print("%0*d/%0*d %s" % (LEN,n,LEN,N,name))
            sqlres = mdb.sql(sql)
            table = dict()
            for res in sqlres:
                _s, _p, _o = re.sub(r'http://data.xed.it/prntt/2016/v1.0/','',res['subject']), \
                             re.sub(r'http://data.xed.it/prntt/2016/v1.0/','',res['predicate']), \
                             res['object']
                if _s not in _els:
                    _els[_s] = defaultdict(list)
                _els[_s][_p].append(str(_o))
            self.tables[name] = _els
            self.pickle(name,_els,'dump')

    def info(self):
        for name,table in sorted(self.tables.items()):
            print("%-20s | %+7s " % (name, len(table)))

    def get(self,reset_cache=False,force=False,verbose=False):
        """
        performance: 1000 subj/sec (from db)  44.000 subj/sec (from cache)
        """
        self.setUp(reset_cache=reset_cache)
        self.info()
        return self.tables[self.TABLE]

    def dump(self,output,limit=2500,reset_cache=False,force=False,verbose=False):
        _els = self.get(reset_cache,force,verbose)
        exporter = KwCorpusExporter(_els)
        if not output:
            output = "output.xml"
        if not re.search('\.xml$',output,re.I):
            output += ".xml"
        print("Save in %s" % output)
        exporter.dump_els_into_xml_files(output, force=force, limit=limit)


def graph_elements_loader(parms, force=False, reset_cache=False,verbose=False):
    db = SpeedGraphDumper(**parms)
    return db.get(reset_cache,force,verbose)

def get_graph_elements_from_db(config, source, force=False, reset_cache=False,verbose=False):
    parms = setup_parms(config,dbname=source)
    return graph_elements_loader(parms, force, reset_cache, verbose)

def speed_dump(parms, force=False, reset_cache=False,verbose=False, limit=2500, output='output.xml'):
    db = SpeedGraphDumper(**parms)
    db.dump(output,limit,reset_cache)

def update_db(db, parms, modified):
    LOGGER.info('Updating db %d elements' % len(modified))
    mdb = MysqlDB(**parms)
    not_exported = []
    with Timer(label='update_db '+db) as timer:
        with Progress(modified.keys(),100,timer=timer,verbose=True) as progress:
            for s,_e1 in modified.items():
                progress.notify(s)
                for p, _v in _e1.items():
                    for o in _v:
                        try:
                            o = re.sub(r"'","\\'",o)
                            sql = "UPDATE %s SET object='%s' WHERE subject='%s' and predicate='%s'" % ('kb_30e064761f_literal_statements',o,str(PRNTT.term(s)),str(PRNTT.term(p)))
                            res = mdb.sql(sql)
                        except Exception as exc:
                            import sys
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            not_exported.append((s+"|"+p,(exc_type,exc_value,exc_traceback)))
    return not_exported
