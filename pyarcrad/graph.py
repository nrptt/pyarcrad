import fnmatch
import hashlib
from collections import defaultdict
from pathlib import Path
from pprint import pformat, pprint

import rdflib.query
from rdflib import Graph, Literal, URIRef, plugin
from rdflib.store import NO_STORE, VALID_STORE, Store

from rdflib_sqlalchemy import registerplugins

from . import LOGGER
from .archive import Archive
from .cursor import MysqlDB
from .db import get_graph_elements_from_db
from .utils import PRNTP, Progress, Timer, add_triples_to_graph, dict_to_triples, setup_parms

registerplugins()

# plugin.register(
#     'sparql', rdflib.query.Processor,
#     'rdfextras.sparql.processor', 'Processor')
# plugin.register(
#     'sparql', rdflib.query.Result,
#     'rdfextras.sparql.query', 'SPARQLQueryResult')



class TripleStore(object):
    ident = URIRef("arcrad")

    def __init__(self, label=None, create=False, glob=None, force=False, config=None, **kwargs):
        self.config = config
        if not label:
            label = "new_store"
        self.label = label
        self.uri = URIRef(label)
        self._graph = None

    @property
    def graph(self):
        if self._graph:
            return self._graph
        LOGGER.info("Opening graph for label %s" % (label))
        with Timer("graph",self.label) as timer:
            self._graph = Graph(identifier=self.ident)
            L = len(self._graph)
            if L>0:
                LOGGER.info('Graph with %d elements (loading time = %3.2f )' % (L, timer.elapsed ))
        

    def elements(self,store,subj=None,pred=None,obj=None,_glob=None,reset_cache=False,_filter=None):
        if not (subj and pred and obj and _glob and _filter):
            if self.config:
                ret = get_graph_elements_from_db(self.config,self.label)
                if ret:
                    return ret
        ns = dict(prntp=PRNTP)
        ffilter = ""
        if _filter:
            ffilter = """FILTER { regex(str(?s), "%s", "i") }""" % _filter
        SPARQL_query = """SELECT ?s ?p ?o WHERE { ?s ?p ?o. %s }""" % (ffilter) 
        initBindings = {}
        if subj:
            sid = URIRef('http://data.xed.it/prntt/2016/v1.0/'+subj)
            initBindings['s'] = sid 
        if obj:
            oid = Literal(obj)
            initBindings['o'] = oid 
        if pred:
            pid = URIRef('http://data.xed.it/prntt/2016/v1.0/'+subj)
            initBindings['p'] = pid 
        _els = {}
        filtered = set()
        for row in store.graph.query(
                 SPARQL_query,
                 initNs=ns,
                 initBindings=initBindings,
                 DEBUG=True):
            _s = row[0][35:]
            _p = row[1][35:]
            _o = row[2]
            if _s not in _els:
                _els[_s] = defaultdict(list)
            _els[_s][_p].append(str(_o))
            if len(_els[_s][_p])>1 and str(_els[_s][_p][-1]).upper() == str(_els[_s][_p][-1]).upper():
                del _els[_s][_p][-1]
            filtered.add(str(_s))
        return _els


    def get_elements(self,store,subj=None,pred=None,obj=None,_glob=None,reset_cache=False,_filter=None,filterout=[]):
        if not (subj and pred and obj and _glob and _filter):
            if self.config:
                ret = get_graph_elements_from_db(self.config,self.label)
                if ret:
                    return ret
        ns = dict(prntp=PRNTP)
        ffilter = ""
        if _filter:
            ffilter = """FILTER { regex(str(?s), "%s", "i") }""" % _filter
        SPARQL_query = """SELECT ?s ?p ?o WHERE { ?s ?p ?o . %s }""" % (ffilter) 
        initBindings = {}
        if obj:
            oid = Literal(obj)
            initBindings['o'] = oid 
        if pred:
            pid = PRNTP.term(pred)
            initBindings['p'] = pid 
        _els = {}
        if subj:
            sid = PRNTP.term(subj)
            initBindings['s'] = sid 
            _els.update(self.query(store,SPARQL_query,ns,initBindings))
        if filterout:
            for subject in filterout:
                sid = URIRef(subject.strip())
                initBindings['s'] = sid 
                _els.update(self.query(store,SPARQL_query,ns,initBindings)) 
        return _els

    def query(self,store,query_object,ns,initBindings,reset_cache=False):
        q =  { 'query_object': query_object,
                'initNs': ns,
                'initBindings':initBindings }
        pq = pformat(q)
        res = None
        if not reset_cache:
            res = self.pickle(pq,None,'load')
        if not res:
            res = store.graph.query(**q)
            self.pickle(pq,res,'dump')
        _els = {}
        for _s, _p, _o in res:
            if _s not in _els:
                _els[_s] = defaultdict(list)
            _els[_s][_p].append(str(_o))
        return _els

    def serialize(self,*args,**kwargs):
        self.graph.serialize(*args,**kwargs)

    def parse(self,*args,**kwargs):
        self.graph.parse(*args,**kwargs)

    def clear(self):
        self.graph.remove((None,None,None))

    def destroy(self):
        self.graph.destroy(self.uri)
        try:
            self.graph.close()
        except:
            pass

    def copy(self,store,timer=None):
        graph = store.graph
        triples = list(graph.triples((None,None,None)))
        LOGGER.info('Selected %d triples from source' % len(triples))
        with Progress(triples,100,timer=timer) as progress:
            for n,triple in enumerate(triples):
                self.graph.add(triple)
                progress.notify(n,True)

    def duplicate(self):
        new_store = TripleStore(label=self.ident)
        with Timer(label="duplicate in memory") as timer:
            new_store.copy(self,timer)
        return new_store

    def add(self, context, triples):
        self._graph = add_triples_to_graph(self.graph, context, triples)

    def add_dict(self,context, _dict):
        self.add(context, dict_to_triples(_dict))

    def statistics(self):
        with Timer("statistics") as timer:
            Any = None
            triples = list(self.graph.triples((Any, Any, Any)))
            subjects = set()
            objects = set()
            predicates = set()
            empties = list()
            for s,p,o in triples:
                subjects.add(s)
                predicates.add(p)
                objects.add(o)
                if len(o)==0:
                    empties.append((s,p))
            stats = dict(
                nfacts = len(triples),
                nfactsReplicate = len(triples) - len(set(triples)),
                nsubjects = len(subjects),
                npredicates = len(predicates),
                npred_subj = len(triples) / len(subjects),
                empty_values = len(empties),
            )
            pprint(stats)

    BASEDIR = Path('~/.pyarcrad_cache').expanduser()

    def pickle(self, query, content, method):
        fname = hashlib.sha224(query.encode('utf-8')).hexdigest()
        tfname = self.BASEDIR / (fname + ".tbl")
        print("PICKLING "+method)
        print(query)
        print(str(tfname))
        if method == 'load':
            func = lambda x,y: pickle.load(x.open('rb'))
        elif method == 'dump':
            if not self.BASEDIR.exists():
                self.BASEDIR.mkdir()
            func = lambda x,y: pickle.dump(y,x.open('wb'))
        else:
            raise ValueError('method unsupported')
        if tfname.exists() or method == 'dump':
            try:
                t = func(tfname,content)
                LOGGER.info("pickle file "+str(tfname)+" "+method+'ed')
            except Exception:
                return
            return t



class SQLAlchemyTripleStore(TripleStore):

    # uri = Literal("mysql+mysqlconnector://root:root@localhost:8889/test_triples")
    dburi = "mysql+mysqlconnector://root:root@localhost:8889/"
    mysql = "mysql+mysqlconnector://root:root@localhost:8889/mysql"

    def __init__(self, label=None, create=False, glob=None, force=False, config=None, **kwargs):
        self.config = config
        if not label:
            raise ValueError('SQLAlchemy label not exists')
        self.label = label
        self.uri = URIRef(self.dburi+label)
        self._graph = None
        self.create = create
        self.force = force

    @property
    def graph(self):
        if self._graph:
            return self._graph
        return self.create_graph()

    def create_graph(self):
        self.store = plugin.get("SQLAlchemy",Store)(identifier=self.ident)
        LOGGER.info("%s graph for label %s" % ("CREATING" if self.create else "OPENING", self.label))
        with Timer("graph",self.label) as timer:
            self._graph = Graph(self.store, identifier=self.ident)
            # workaround for create bug
            if self.create:
                db = MysqlDB(url=self.uri)
                if db.exists():
                    if self.force:
                        db.drop_db(self.label)
                    else:
                        raise ValueError('Cannot delete existent DB %s without force' % self.label)
                db = MysqlDB(url=self.mysql)
                db.create_db(self.label)
            rt = self._graph.open(self.uri, create=self.create)            
            if rt == NO_STORE:
                # There is no underlying Sleepycat infrastructure, create it
                self._graph.open(self.uri, create=True)
            else:
                assert rt == VALID_STORE, "The underlying store is corrupt"
            L = len(self._graph)
            if self.create:
                LOGGER.info('Clearing graph with %d elements (loading time = %3.2f )' % (L, timer.elapsed ))
                self._graph.remove((None,None,None))
            if L>0:
                LOGGER.info('Graph with %d elements (loading time = %3.2f )' % (L, timer.elapsed ))
        return self._graph

class TripleStoreUploader(object):

    def __init__(self, triplestore):
        self.triplestore = triplestore

    def upload(self, defs):
        print("Uploading store")
        pprint(defs)
        with Archive(defs,self.triplestore) as archive:
            triples = archive.triples
            # self.triplestore.add('test',triples) 


def upload_archive(defs,**kwargs):
    store = SQLAlchemyTripleStore(**kwargs)
    tsu = TripleStoreUploader(store)
    tsu.upload(defs)
    return store


def load_archive(**kwargs):
    """load a graph archive
    store = load_archive(label='arcrad_exag')
    """
    return SQLAlchemyTripleStore(**kwargs)
