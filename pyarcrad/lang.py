import argparse
import os
import re
import sys
import tempfile

import polyglot
import pywikibot
from bs4 import BeautifulSoup
from pywikibot import i18n


class ArticleEditor(object):

    """Edit a wiki page."""

    # join lines if line starts with this ones
    # TODO: No apparent usage
    # joinchars = string.letters + '[]' + string.digits

    def __init__(self, *args):
        self.set_options(*args)
        # self.setpage()
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)

        parser = argparse.ArgumentParser(add_help=False)
        parser.add_argument("-r", "--edit_redirect", "--edit-redirect",
                            action="store_true", help="Ignore/edit redirects")
        parser.add_argument("-p", "--page", help="Page to edit")
        parser.add_argument("-w", "--watch", action="store_true",
                            help="Watch article after edit")
        # convenience positional argument so we can act like a normal editor
        parser.add_argument("wikipage", nargs="?", help="Page to edit")
        self.options = parser.parse_args(my_args)

        if self.options.page and self.options.wikipage:
            pywikibot.error(u"Multiple pages passed. Please specify a single page to edit.")
            sys.exit(1)
        self.options.page = self.options.page or self.options.wikipage

    def setpage(self):
        """Set page and page title."""
        site = pywikibot.Site()
        pageTitle = self.options.page or pywikibot.input(u"Page to edit:")
        self.page = pywikibot.Page(pywikibot.Link(pageTitle, site))
        if not self.options.edit_redirect and self.page.isRedirectPage():
            self.page = self.page.getRedirectTarget()

    def handle_edit_conflict(self, new):
        fn = os.path.join(tempfile.gettempdir(), self.page.title())
        fp = open(fn, 'w')
        fp.write(new)
        fp.close()
        pywikibot.output(
            u"An edit conflict has arisen. Your edit has been saved to %s. Please try again."
            % fn)

    def load(self, page):
        self.options.page = page
        self.setpage()
        self.site.login()
        return self.page.get(get_redirect=self.options.edit_redirect)

    def get(self, page):
        self.page = self.load(page)
        self.info = dict([
            map(lambda x: x.strip(), 
                re.split('=',x.strip(),maxsplit=1)) if '=' in x else ['TEMPLATE', x] 
            for x in re.sub(r'\{\{(.+?)\}\}.*$','\\1', re.sub(r'\n',' ', self.page)).split('|')])
        self.h_text = re.sub(r'§-§','\\n',re.sub(r'{{.+?}}',' ',re.sub('\n','§-§', self.page), 
                                                 re.M +  re.DEBUG).strip())
        self.soup = BeautifulSoup(self.h_text,"lxml")
        self.raw_text = self.soup.get_text()
        self.raw_text = re.sub(r'\[\[Category:.*?\]\]','',self.raw_text) # remove Categories

    @property
    def raw(self):
        return self.raw_text

    @property
    def html(self):
        return self.h_text

    def save(self,new,old):
        if new and old != new:
            pywikibot.showDiff(old, new)
            changes = pywikibot.input(u"What did you change?")
            comment = i18n.twtranslate(pywikibot.Site(), 'editarticle-edit',
                                       {'description': changes})
            try:
                self.page.put(new, summary=comment, minorEdit=False,
                              watchArticle=self.options.watch)
            except pywikibot.EditConflict:
                self.handle_edit_conflict(new)
        else:
            pywikibot.output(u"Nothing changed")



class ARPolyglot(object):

    def __init__(self,page):
        pywikibot.output(u'getting page %s' % page)
        self.title = page
        self.archive = ArticleEditor()
        self.archive.get(page)

    def analyze(self):
        from polyglot.downloader import downloader
        # downloader.download("LANG:it")
        # downloader.download("LANG:en")
        # downloader.download("LANG:es")
        # downloader.download("LANG:de")
        from polyglot.detect import Detector
        raw = self.archive.raw
        ptext = Detector(raw)
        print(ptext.language)
        from polyglot.text import Text
        ttext = Text(raw)
        print(ttext.language)
        print(ttext.words[:100])
        print(ttext.sentences[:4])
        print(ttext.pos_tags[:20])
        print("{:<16}{}".format("Word", "Polarity")+"\n"+"-"*30)
        for w in ttext.words:
            if w.polarity:
                print("{:<16}{:>2}".format(w, w.polarity))
        for e in ttext.entities:            
            print(e,e.positive_sentiment, e.negative_sentiment)
        print(ttext.entities)
        import pdb; pdb.set_trace()
        from polyglot.mapping.base import CountedVocabulary

# infobox = dict([ re.split('=',x.strip(),maxsplit=1) if '=' in x else ['TEMPLATE', x] for x in re.sub(r'\{\{(.+?)\}\}.*$','\\1', re.sub(r'\n',' ', text)).split('|')])


# re.sub(r'\{\{(.+)\}\}.*$','\\1', re.sub(r'\n',' ', text))
# infobox = dict([ map(lambda x: x.strip(),re.split('=',x,maxsplit=1)) if '=' in x else ('TEMPLATE', x ) for x in re.sub(r'\{\{(.+)\}\}.*$','\\1', re.sub(r'\n',' ', text)).split('|')])
